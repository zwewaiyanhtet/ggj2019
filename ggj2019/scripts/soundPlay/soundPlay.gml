/// @desc Play a sound clip once.
/// @arg sound_id

audio_play_sound(argument0, 1, false);