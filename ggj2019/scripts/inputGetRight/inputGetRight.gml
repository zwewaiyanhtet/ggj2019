/// @desc Input direction right.
/// @return int 1 or 0
return keyboard_check(vk_right) || keyboard_check(ord("D"))