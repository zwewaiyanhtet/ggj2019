/// @desc Room - Setup switch with a door or an elevator.
/// @arg target Target door instance ID in the room. 
/// @arg [doorOpened] Target door state. true is open, false is close which is default

target = argument[0];
if(argument_count > 1) {
	targetOpened = argument[1];	
}