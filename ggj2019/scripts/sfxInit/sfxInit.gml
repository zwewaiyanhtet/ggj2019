global.bgMusic1 = musBackground1;
global.bgMusic2 = noone;

global.playerPopInSound = sndPlayerPopIn;
global.playerPopOutSound = sndPlayerPopOut;
global.playerDeathSound = sndPlayerDeath;

global.spikeDropSound = sndSpikeDrop;
global.switchActivateSound = sndSwitchAct;
global.switchDeactivateSound = sndSwitchDeact;