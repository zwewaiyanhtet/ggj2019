/// @desc Setup elevator vertical movement distance. 
/// @arg distance Distance to update
/// @arg [vDir] -1 for up by default. 1 for down.

moveDistance = argument[0];
distanceLeft = moveDistance;

if argument_count > 1 {
	vDir = argument[1];
}