/// @desc Room - Setup switch to be followed on an elevator.
/// @arg target Target switch instance ID in the room. 
/// @arg handle Switch object to be handled by the elevator. 
/// @arg moveWithElevator False if the switch movement doesn't need to be handled by the handler elevator.
/// @arg [doorOpened] true if the target is a door and you want the door state to be opened at the start

target = argument[0];
target.handle = argument[1];
moveWithElevator = argument[2];
if(argument_count > 3 && object_get_name(target.object_index) == "oDoor")  targetOpened = argument[3];