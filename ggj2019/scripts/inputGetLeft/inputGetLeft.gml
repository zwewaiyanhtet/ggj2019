/// @desc Input direction left.
/// @return int 1 or 0
return keyboard_check(vk_left) || keyboard_check(ord("A"))