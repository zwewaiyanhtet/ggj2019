/// @desc Burst play the spike particles at the given position.
/// @arg x
/// @arg y

/// e.g., spikePartsPlay(mouse_x, mouse_y);

var xx = argument0;
var yy = argument1;
with oSpikePartsGenerator {
	part_system_position(partSystem, xx, yy);
	part_emitter_burst(partSystem, partEmitter, partType, irandom_range(2, 4));
}