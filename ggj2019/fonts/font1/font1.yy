{
    "id": "2b6e9f9c-c2ff-48ae-ba65-803ab9694f21",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "font1",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Arabic Typesetting",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "feb78ee7-7f82-4dea-a43d-b030819731aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 55,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "c2999db4-a19e-41e4-95e0-97992f7cd457",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 55,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 170,
                "y": 116
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "16ee66aa-998b-4c5b-9c4f-e9a6ef71f894",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 55,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 158,
                "y": 116
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "65c90d9c-964d-45ef-b4ad-1f4bd28b5c94",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 55,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 140,
                "y": 116
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "dda290b7-3c10-438d-a140-d24149b53c28",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 55,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 124,
                "y": 116
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "e9b87e80-b40c-4231-8e8e-0bb67e9e4209",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 55,
                "offset": 2,
                "shift": 30,
                "w": 26,
                "x": 96,
                "y": 116
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "9d048b80-d8ea-49fd-85e6-fca79432c88f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 55,
                "offset": 1,
                "shift": 25,
                "w": 23,
                "x": 71,
                "y": 116
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "e74b9ef6-b4fa-4505-bc39-ee88fec96d17",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 55,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 65,
                "y": 116
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "e360c443-d1e5-405d-b0eb-8c0d531028d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 55,
                "offset": 1,
                "shift": 12,
                "w": 9,
                "x": 54,
                "y": 116
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "04866d9e-6d24-4b99-9bbe-c81ef52cc47c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 55,
                "offset": 2,
                "shift": 12,
                "w": 9,
                "x": 43,
                "y": 116
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "744b3dc8-327e-40fa-a6d2-6f5c42a4cb9c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 55,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 177,
                "y": 116
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "dd217a3b-097c-4cfa-985b-da1b3e213fe7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 55,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 27,
                "y": 116
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "22d39802-761b-4ed0-99eb-e2954711da8f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 55,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 2,
                "y": 116
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "19bb0a11-ea02-4703-b4fc-673fb1df10e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 55,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 496,
                "y": 59
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "ca415c32-456f-4715-b232-1a2d24d9100d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 55,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 490,
                "y": 59
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "46567fee-a2bd-4e26-a398-ffa05b208dfd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 55,
                "offset": 1,
                "shift": 13,
                "w": 10,
                "x": 478,
                "y": 59
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "3e6eb34b-e94e-47f7-8117-671e0f7d726b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 55,
                "offset": 0,
                "shift": 16,
                "w": 15,
                "x": 461,
                "y": 59
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "e16a03e4-a9bf-4bab-ac48-00faf1bc0e60",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 55,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 447,
                "y": 59
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "8f9bafc0-a113-46a0-aa13-ea9e98ab9d1d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 55,
                "offset": 0,
                "shift": 16,
                "w": 15,
                "x": 430,
                "y": 59
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "b40d1a8c-2694-4a05-9bc8-625ca29ace02",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 55,
                "offset": 0,
                "shift": 16,
                "w": 15,
                "x": 413,
                "y": 59
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "6f779635-fcf4-45c3-bda4-2c4fad264cbd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 55,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 395,
                "y": 59
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "b011dd09-033c-47cd-a4a5-4312288c90fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 55,
                "offset": 0,
                "shift": 16,
                "w": 15,
                "x": 10,
                "y": 116
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "dcec52da-db68-4959-acfe-8f354133c4b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 55,
                "offset": 0,
                "shift": 16,
                "w": 15,
                "x": 189,
                "y": 116
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "a4d83d0b-6b4b-4480-bba2-9b3ad28dcf1b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 55,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 206,
                "y": 116
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "45205ed8-72fe-4882-8a1c-b6dfa2b407dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 55,
                "offset": 0,
                "shift": 16,
                "w": 15,
                "x": 223,
                "y": 116
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "31679032-cc3a-46de-b4a6-33bd25185573",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 55,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 130,
                "y": 173
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "486fa2a2-0c86-4238-8043-902a3e861e5e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 55,
                "offset": 3,
                "shift": 8,
                "w": 4,
                "x": 124,
                "y": 173
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "7e01cb67-6cb4-4153-af80-0284f0683e5e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 55,
                "offset": 2,
                "shift": 8,
                "w": 6,
                "x": 116,
                "y": 173
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "3b4e67ff-4549-43a5-8c46-b794541c928c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 55,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 100,
                "y": 173
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "b736010b-585d-4b12-95cf-7297893d7cbc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 55,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 84,
                "y": 173
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "dad109cb-5533-4a6d-b110-21a5abefa8cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 55,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 68,
                "y": 173
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "3e4da434-1f88-4c71-97e0-9b6efd21b649",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 55,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 56,
                "y": 173
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "6ba84319-5c4c-4059-b57e-02e7c4020333",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 55,
                "offset": 1,
                "shift": 28,
                "w": 26,
                "x": 28,
                "y": 173
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "f5afd2df-5ef0-4eac-8229-1100dbba718f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 55,
                "offset": -1,
                "shift": 23,
                "w": 24,
                "x": 2,
                "y": 173
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "309e2737-6b86-44f1-9375-5841f8c358fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 55,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 481,
                "y": 116
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "914fd0b3-d0e3-442f-871a-a6a92450f34d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 55,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 457,
                "y": 116
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "7e47ffd1-29f3-4b92-86e4-8656d4fa7935",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 55,
                "offset": 1,
                "shift": 25,
                "w": 23,
                "x": 432,
                "y": 116
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "accdb75b-0e75-466d-972f-61d454c6c313",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 55,
                "offset": 1,
                "shift": 19,
                "w": 18,
                "x": 412,
                "y": 116
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "bb1e7968-4bbb-4e5f-860d-61c991917fad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 55,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 394,
                "y": 116
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "c0e8dd1d-b2c2-4864-9ceb-9ba7db6c1e88",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 55,
                "offset": 1,
                "shift": 25,
                "w": 23,
                "x": 369,
                "y": 116
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "6a9b955d-ff56-4897-9f8b-eba66726a32e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 55,
                "offset": 1,
                "shift": 27,
                "w": 25,
                "x": 342,
                "y": 116
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "5493369c-7e2f-4a18-b27f-8c61e0687ccd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 55,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 330,
                "y": 116
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "a08f6fef-f327-4dc6-8752-19e67fc2e011",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 55,
                "offset": -3,
                "shift": 11,
                "w": 13,
                "x": 315,
                "y": 116
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "aac8057f-b4a1-4148-83a1-4a3a06005e75",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 55,
                "offset": 1,
                "shift": 23,
                "w": 22,
                "x": 291,
                "y": 116
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "0ff61f8c-8632-425b-8870-1e48621b5f5c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 55,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 272,
                "y": 116
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "a1630c6c-8752-44b1-a7ea-8b60aebeb8ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 55,
                "offset": 0,
                "shift": 30,
                "w": 30,
                "x": 240,
                "y": 116
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "b5674916-dce8-4a35-a5fd-5df0239a9203",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 55,
                "offset": 0,
                "shift": 26,
                "w": 25,
                "x": 368,
                "y": 59
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "3ff2be7a-a19b-43d3-8efd-aad41423ef87",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 55,
                "offset": 1,
                "shift": 26,
                "w": 24,
                "x": 342,
                "y": 59
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "f7c4bd16-e6f5-46e2-9280-16a70dbdd737",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 55,
                "offset": 1,
                "shift": 19,
                "w": 18,
                "x": 322,
                "y": 59
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "5de621ef-a97d-4879-85e4-60ab113e38b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 55,
                "offset": 1,
                "shift": 26,
                "w": 33,
                "x": 417,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "7bf4d40f-36dc-4b70-9ac4-982471b3c372",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 55,
                "offset": 1,
                "shift": 21,
                "w": 21,
                "x": 384,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "9c4a1769-0e08-46d9-9a82-5242b260541d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 55,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 366,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "5e8897aa-3027-4a87-8e81-af4b60fbbdf1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 55,
                "offset": 0,
                "shift": 21,
                "w": 20,
                "x": 344,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "70835a18-82e1-4408-8eaa-5f39fda53165",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 55,
                "offset": 0,
                "shift": 25,
                "w": 25,
                "x": 317,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "2ce7b798-eb19-4579-a6a7-d53f877894cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 55,
                "offset": -1,
                "shift": 23,
                "w": 25,
                "x": 290,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "c0b9ceb9-d6b8-4c76-9cd2-951f8306143b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 55,
                "offset": -1,
                "shift": 32,
                "w": 34,
                "x": 254,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "7960746f-81c9-4a88-a9a9-0ed29d1bfcd1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 55,
                "offset": -1,
                "shift": 23,
                "w": 24,
                "x": 228,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "57ff40a1-af04-4ce8-adf5-c23fbba3e34a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 55,
                "offset": -1,
                "shift": 20,
                "w": 22,
                "x": 204,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "653fa16a-4396-43df-a14e-e0a521e83ec1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 55,
                "offset": 1,
                "shift": 21,
                "w": 20,
                "x": 182,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "2bc1dd10-e2f4-4580-854b-dda638f606ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 55,
                "offset": 2,
                "shift": 12,
                "w": 8,
                "x": 407,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "bc4f38f5-5442-43d7-9d4b-cffa81af1baf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 55,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 170,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "0fbf697a-761b-4207-af31-22ad2bbca543",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 55,
                "offset": 2,
                "shift": 12,
                "w": 7,
                "x": 145,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "f87a316a-46c8-48f4-85ed-76fecdc63d31",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 55,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 129,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "9b5f4af7-f4f3-4416-84c7-4f5030e40b46",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 55,
                "offset": 0,
                "shift": 24,
                "w": 24,
                "x": 103,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "4380dfb0-5c90-4322-b8b6-f4555841458c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 55,
                "offset": 4,
                "shift": 16,
                "w": 5,
                "x": 96,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "8cb9ec46-8e25-4f16-a8af-bc74e09baf2f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 55,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 80,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "9ca0713c-2d6e-4ec6-ade7-a408ccabcd3d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 55,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 62,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "e629088f-ec2a-422f-abda-b3d415dab473",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 55,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 47,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "b9cda247-cc71-4eb9-a94a-7555a4605b0d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 55,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 29,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "28600241-92f9-4b64-abbf-26430626c29f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 55,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 14,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "c109400b-3ec1-4527-afc9-9234a90b5ac6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 55,
                "offset": 0,
                "shift": 10,
                "w": 14,
                "x": 154,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "4b4f9e78-a17c-4556-8044-4b615693afb4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 55,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 452,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "81ecb064-4096-423c-ae84-dd4226024432",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 55,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 121,
                "y": 59
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "5bd1b37d-8e4f-458b-8014-9e082158afd1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 55,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 470,
                "y": 2
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "87d44d84-da79-4613-af9a-57124b929020",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 55,
                "offset": -1,
                "shift": 8,
                "w": 8,
                "x": 299,
                "y": 59
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "2a61621f-b96d-4a9e-bd29-9da01aceabed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 55,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 280,
                "y": 59
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "7b11edfd-67fd-4d1a-b52c-9e4c41a40f43",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 55,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 269,
                "y": 59
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "c94d2c5e-b654-4be2-b3f7-593f858c7a3a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 55,
                "offset": 0,
                "shift": 27,
                "w": 27,
                "x": 240,
                "y": 59
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "8b0bbcb0-a04b-40ff-aed3-ed8f64eb494c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 55,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 220,
                "y": 59
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "d38755c2-afb5-4a9e-a167-fe791c7bd8ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 55,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 203,
                "y": 59
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "5ee247f1-fa2f-456c-b63a-4f05cc036ad5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 55,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 184,
                "y": 59
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "40983e1d-1d43-403a-b41f-7553701a1097",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 55,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 166,
                "y": 59
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "0041d1e7-e0b9-4dbc-8a2c-df38d0fc4f89",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 55,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 153,
                "y": 59
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "b5cfa721-7262-49c8-ae49-77fa213610ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 55,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 309,
                "y": 59
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "e78a2419-b7e2-4d0e-9442-bd9842638ff0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 55,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 141,
                "y": 59
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "e05a8e3e-cb04-488a-9a1a-1d6a231cab06",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 55,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 101,
                "y": 59
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "8a414093-2f42-42bf-b1e7-8f757f1c4d09",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 55,
                "offset": -1,
                "shift": 15,
                "w": 17,
                "x": 82,
                "y": 59
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "659ca944-97c1-4855-9094-9772fd2dca28",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 55,
                "offset": 0,
                "shift": 23,
                "w": 24,
                "x": 56,
                "y": 59
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "e57b9262-d25c-40e9-a028-d450d6cc86c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 55,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 38,
                "y": 59
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "fa0da7f2-da80-48db-acd2-f1373c7d5773",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 55,
                "offset": -1,
                "shift": 16,
                "w": 18,
                "x": 18,
                "y": 59
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "bc1f110a-3446-4fba-9be9-7e35bb9f1220",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 55,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 2,
                "y": 59
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "47df09fd-eaf7-46ad-b4da-597e7fc7692d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 55,
                "offset": 0,
                "shift": 12,
                "w": 9,
                "x": 498,
                "y": 2
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "1987d304-d7f4-4afe-b85c-43e538f14226",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 55,
                "offset": 3,
                "shift": 9,
                "w": 3,
                "x": 493,
                "y": 2
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "ba0c647d-e1f8-4f88-8120-ff42b51524c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 55,
                "offset": 2,
                "shift": 12,
                "w": 10,
                "x": 481,
                "y": 2
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "afe386d1-26a4-4c56-8532-464dcdb090a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 55,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 148,
                "y": 173
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "7d33d58e-d505-4606-9705-ee1558b745cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 55,
                "offset": 9,
                "shift": 29,
                "w": 11,
                "x": 164,
                "y": 173
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000a\\u000aDefault Character(9647) ▯",
    "size": 36,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}