{
    "id": "42c493b1-ad6b-49a8-9000-52ceade3b49e",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "font0",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Arial Rounded MT Bold",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "68179a81-33a1-45fe-820e-e960611445af",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 19,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "be74f6e9-f14f-4fab-b768-5a590d3b473a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 19,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 194,
                "y": 44
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "0e0b99ee-a0b4-4b2a-b79f-968f78f5bd14",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 185,
                "y": 44
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "b53ba307-8f12-4b20-9d35-cee437e1d2ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 174,
                "y": 44
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "f580779c-c0b5-40d8-b032-c5d021364326",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 163,
                "y": 44
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "1f36dbd0-e5fb-43b6-ba69-af0aade68632",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 19,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 147,
                "y": 44
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "3915f557-2ede-42da-8040-21b2b6b0afba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 19,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 133,
                "y": 44
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "1013f430-0325-4bff-8fa4-c479d67dbb0c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 19,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 127,
                "y": 44
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "31fdc8ea-fbaf-436b-b7e7-99d6b6eaf05c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 19,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 121,
                "y": 44
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "748df66f-5b43-4209-9f6c-c59f50005dcf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 114,
                "y": 44
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "bdabb150-595c-4afa-b119-0933eedf5ad4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 199,
                "y": 44
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "a495c3cc-cf1c-4d49-bd54-05b82ef96e2c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 103,
                "y": 44
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "a01c0d26-9a92-4ad3-b48c-8f46a766ff1f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 19,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 87,
                "y": 44
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "df2181ad-9bac-4202-b7e4-a13921a3ed0f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 19,
                "offset": 0,
                "shift": 5,
                "w": 6,
                "x": 79,
                "y": 44
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "96c4100a-03d7-47f9-a26e-cf10cddb3c3f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 19,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 74,
                "y": 44
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "28568e5b-551a-4232-9f9a-b4eab9e27408",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 19,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 67,
                "y": 44
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "7b4c078c-0eff-4527-a56d-16b888f81ba1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 56,
                "y": 44
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "670a0311-1def-4017-b751-9b13f4494bbf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 19,
                "offset": 1,
                "shift": 10,
                "w": 6,
                "x": 48,
                "y": 44
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "a7ce72ba-20fc-462d-8041-8260fce2f95e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 37,
                "y": 44
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "c0bbca15-5fa7-44b8-98bb-275e9e0dc0be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 26,
                "y": 44
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "604f389f-b878-4dee-94dd-6529de938fcd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 14,
                "y": 44
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "4ba872d2-690a-459b-93c0-1885decce07f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 92,
                "y": 44
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "ad8e7d10-06cb-4e93-b8cb-b7cede6353b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 208,
                "y": 44
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "01e30d41-77eb-4681-8067-963bb8f96805",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 19,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 219,
                "y": 44
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "9fd9eb17-5e39-4eca-a170-437684cc82ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 230,
                "y": 44
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "b8b547a6-65d4-4f36-9141-01cfd5582729",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 215,
                "y": 65
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "c8e6b537-daf3-425c-ae7f-2105ebbfb361",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 19,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 210,
                "y": 65
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "4d496475-7a98-4d60-a0ed-8e7c359e077f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 19,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 205,
                "y": 65
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "427adcbf-d714-405e-8cfd-23dd6b96f9da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 194,
                "y": 65
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "4e40ba5b-7d14-4889-ac6f-4058fad1f757",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 183,
                "y": 65
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "a4b1c019-ad03-44d2-adf8-f6c08e8a1b0f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 172,
                "y": 65
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "d44d3659-006a-4067-bf9e-c0b6279d02c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 161,
                "y": 65
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "76050210-92f7-440d-851b-3b2523c3b0b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 19,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 143,
                "y": 65
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "612e9a2a-73c0-4b68-b0cc-97b659d93eec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 19,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 129,
                "y": 65
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "d9cfd45a-330d-4988-9dc9-fbcee8df23e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 19,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 117,
                "y": 65
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "f88259cf-6222-4338-be65-7ba3e93733e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 19,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 103,
                "y": 65
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "af1ac583-0871-45ad-beb5-2b1e2649b601",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 19,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 90,
                "y": 65
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "f6062368-7eeb-427e-9f57-d80de57b8af4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 19,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 78,
                "y": 65
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "38d05f36-5432-49ea-833e-f23db878a8dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 19,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 67,
                "y": 65
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "522675fd-bdad-4e34-88ad-d5b3bf7bd8d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 19,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 53,
                "y": 65
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "505bfcfc-e006-475c-b91f-27020319ae30",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 19,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 41,
                "y": 65
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "d1e9a364-fd2a-4085-bd28-b07ffa2a3ab7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 19,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 36,
                "y": 65
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "1cea16c8-1b95-41e7-8f2e-6e381b9b025c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 26,
                "y": 65
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "a54ba242-35f3-4822-a6f4-48ad12c3b9d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 19,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 13,
                "y": 65
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "6cd5c2fd-86fe-457f-a121-e593aeffcedc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 19,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 2,
                "y": 65
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "6f925c51-bf4d-4369-a0cf-ad4164d99132",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 19,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 241,
                "y": 44
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "f0261784-24c0-4729-8c35-8153eeaa5733",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 19,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 2,
                "y": 44
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "1a4a7132-6948-4e3e-92e9-1ac7be86b7d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 19,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 237,
                "y": 23
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "e3a2ce22-499a-4e68-a34e-f0b04c9ff9ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 19,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 226,
                "y": 23
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "31d52277-052a-424d-9edc-9f6c60df5249",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 19,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 238,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "dbbddcf7-a872-4a91-9b22-06813a49ea56",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 19,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 219,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "87fa36bb-7b49-40c3-bacb-3b189f39a422",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 19,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 207,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "f1e05ea5-7aed-420b-89df-91b2d043a00f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 195,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "427cf528-a124-44eb-87a8-620c4c261a4e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 19,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 183,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "92a6d328-3ef0-46a8-bd54-0b9ddefb95e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 19,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 170,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "e361f6eb-e045-4772-adcb-6dedfa6d49aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 19,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 153,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "12e00e68-16ec-4ca6-8268-b3273014abb3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 141,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "bc60ad54-ca19-495c-b793-538eaf102155",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 129,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "ce94fa2b-3fe2-48ff-84d2-42f59bc40abc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 116,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "da2e1027-436d-467d-a68a-118db293aae5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 19,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 231,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "77a1dbc0-f6b6-461a-b5e8-c2e1d4eb3769",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 19,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 109,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "21af1c7a-d07b-4d0a-8ffb-1602b2728dd8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 92,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "6c88f267-cb86-4732-a011-0c9becf506c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 81,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "03c44911-e75c-4e0a-92cc-aa3e7167eb7a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 19,
                "offset": -1,
                "shift": 8,
                "w": 10,
                "x": 69,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "57b7b49b-f2c7-4e96-b10d-7e77347e88fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 19,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 63,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "0a15ebe3-a945-4ab0-b8c0-9d5f2cf99798",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 52,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "63ad6513-82c5-4041-b7cc-586034d63747",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 19,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 41,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "d283c66f-00bb-44b7-b809-ff0dad0d0ce4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 30,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "df0f0fb7-53ab-4a2e-922f-7f528f2907eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 19,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "ef33d9e1-80fe-40e5-8801-e31b5b4ea9db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 8,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "c1817f92-91ea-431e-bbf6-dfcf60e5561c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 19,
                "offset": -1,
                "shift": 5,
                "w": 8,
                "x": 99,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "f62110c6-f21d-4ad6-9019-90a36abbafa4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 2,
                "y": 23
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "f43d2039-19a7-4d77-8ccf-d877ebd0d139",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 19,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 107,
                "y": 23
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "92acecc5-5495-4418-9e93-e3b725035f18",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 19,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 13,
                "y": 23
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "13b3b093-ea62-4cdf-88cb-22f7c81d19a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 19,
                "offset": -2,
                "shift": 4,
                "w": 6,
                "x": 208,
                "y": 23
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "bb5b9f8e-b8de-4bd7-831a-ed3242b7b979",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 198,
                "y": 23
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "8baea35c-203b-4a31-a9df-70a199da978e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 19,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 193,
                "y": 23
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "f8864b24-f6a6-4652-93a8-bd7c2705a1f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 19,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 177,
                "y": 23
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "1a869599-adf8-4689-bdde-85c7384c3fba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 19,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 167,
                "y": 23
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "c25b52d7-c75c-4d9e-8aed-c79c17b21faa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 155,
                "y": 23
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "90dc46b2-a79c-4608-9bce-3e427a72ae86",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 19,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 144,
                "y": 23
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "7e083a88-34c1-4908-9ef3-5b05898e4a84",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 133,
                "y": 23
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "e829816e-85d2-48a5-910a-7d0c414618cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 19,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 125,
                "y": 23
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "b089c12d-517c-4ca6-9912-32934090311a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 216,
                "y": 23
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "7748b8c5-54bb-44b1-a53c-628e5a1ba793",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 117,
                "y": 23
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "bafb0030-da24-422f-95d0-68e7b76ab530",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 19,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 97,
                "y": 23
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "288350cf-9be0-49fa-a88e-4dcfd14a6dfc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 86,
                "y": 23
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "163e5d17-8e84-4f77-90d1-59ede6748c20",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 19,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 71,
                "y": 23
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "cd576f49-9506-44b3-8127-2050652ffe18",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 61,
                "y": 23
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "6cdbe7cc-2559-4898-b844-ff69f2d534ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 50,
                "y": 23
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "4853fd98-c360-4bf5-81e4-4461416a3e69",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 9,
                "x": 39,
                "y": 23
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "1a316a01-921b-4008-80c9-12163474786e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 31,
                "y": 23
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "28414f12-c822-4687-a6ef-5ca45de0cc3b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 19,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 26,
                "y": 23
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "19fd4c7c-8300-49a9-875b-08eb6cf70209",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 18,
                "y": 23
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "12dc8b47-7fa8-44b9-ae3e-0d9209254ba5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 226,
                "y": 65
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "9d84a8fc-8e8e-486f-842d-bea9f4df528b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 19,
                "offset": 3,
                "shift": 10,
                "w": 4,
                "x": 237,
                "y": 65
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        {
            "id": "df39703e-820c-4c10-bbc2-2dc8a0b5c7bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 40,
            "second": 106
        },
        {
            "id": "dcb65c04-c0a4-4394-91d0-0df94fa13412",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 84
        },
        {
            "id": "139627e9-e6f8-4af1-af51-660052039d0a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 86
        },
        {
            "id": "675f5233-ec04-4b76-be3b-e20cedb49647",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 89
        },
        {
            "id": "6183f4f3-dbac-4444-941b-f773be09327e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 46
        },
        {
            "id": "0e6b57b1-2a2e-412a-802a-b5fe5d13fc74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 46
        },
        {
            "id": "6ddb13a9-026d-4739-adc4-a3c8cc7f9343",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 44
        },
        {
            "id": "595a3127-d2d3-4a45-8462-0d6f604090d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 46
        },
        {
            "id": "5be79400-c52c-4eb3-8ec7-cc93f180bee3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 44
        },
        {
            "id": "8deb2f33-c673-463d-9ee7-71022f6160ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 46
        },
        {
            "id": "c9774cf5-156d-4b3c-a374-dc24d70e9025",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 65
        },
        {
            "id": "7d5b8218-4a58-40fc-9ab7-3ef4e1d95672",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 196
        },
        {
            "id": "e1a229c6-2cd9-44f8-b608-4182f2297bf3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 197
        },
        {
            "id": "dca0c46a-4d5e-4e03-b047-917ad6877dd7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 44
        },
        {
            "id": "1b62b9b1-1161-4a99-8044-493ed2c9728c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 46
        },
        {
            "id": "f571c27c-88d6-405b-898b-3fa0e03553a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 67
        },
        {
            "id": "8d7b4cf9-dc81-498f-9fba-a09e6d7612b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 71
        },
        {
            "id": "83393566-848c-4743-8c34-a29361fce6db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 118
        },
        {
            "id": "288975a3-94e2-47c9-8a27-b65906ca776a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 119
        },
        {
            "id": "c3b4db15-c5f6-441a-b290-d1218b009955",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 67
        },
        {
            "id": "de33d893-60e5-4f8c-a5cc-be07d925f0ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 71
        },
        {
            "id": "8b4c3f87-f7b0-4137-b69d-08239ebe5c19",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 79
        },
        {
            "id": "3d9d0456-6226-43e1-89c2-84aab39d86b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 84
        },
        {
            "id": "b10a2d82-cf66-4242-84e7-85c202066673",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 85
        },
        {
            "id": "1a76a82a-3a5d-40c9-98c0-880853bdf0bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 86
        },
        {
            "id": "b3f309e4-bb10-4f19-9292-18eaa231fb2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 87
        },
        {
            "id": "51239dd3-3da8-4956-938c-40762f43a1b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 89
        },
        {
            "id": "e99690f4-4bb1-4556-b1ae-0e72d10d6b6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 214
        },
        {
            "id": "5593e8d0-094c-433c-935a-812b3420180b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 220
        },
        {
            "id": "b0ea55f4-6d79-4888-b3ec-5b0ac04b5da8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8217
        },
        {
            "id": "0847c024-9d3c-4d60-be6f-8f8f5a34af32",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8221
        },
        {
            "id": "b2a7f91e-91c3-40d1-bb8e-0d03af6038a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 46
        },
        {
            "id": "689b2b58-3b52-4f27-a8c8-4824857100bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 44
        },
        {
            "id": "bba833fc-336f-43e1-806a-574459ad07f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 46
        },
        {
            "id": "3814f330-8571-4daa-8264-89d6c519b1d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 65
        },
        {
            "id": "eee7699d-9ab8-4ad2-be88-544087a8a912",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 196
        },
        {
            "id": "aacd16d0-d822-429b-9eb8-2d1c518bc1bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 197
        },
        {
            "id": "7da935e5-b769-4b73-9a8e-fe509405baca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 89
        },
        {
            "id": "8bdc41aa-abea-41d8-bab9-e71e6ce88104",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 81,
            "second": 106
        },
        {
            "id": "5ecf4004-1146-409f-a03a-76d4d4df7451",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 84
        },
        {
            "id": "b0c73d4d-92d9-4e8f-9851-0ffd6ace1def",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 44
        },
        {
            "id": "fec788fe-0e6e-4282-9d84-3d18980248c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 45
        },
        {
            "id": "5ecc17f3-c29b-42fc-818b-25446d4093fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 46
        },
        {
            "id": "5be3f01a-6d5c-4a14-91d1-943d97a330a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 58
        },
        {
            "id": "d5fdbb56-1d6e-40b2-a958-285730f077d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 59
        },
        {
            "id": "80389721-6c81-44e6-bb92-29ef584168f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 65
        },
        {
            "id": "2eb9cbe1-63c7-451e-80ed-312d973757fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 97
        },
        {
            "id": "2f419692-2081-41ff-8fce-6c354087877d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 99
        },
        {
            "id": "48c2c6ca-aef2-4846-8c7c-76ee9ea0d29c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 101
        },
        {
            "id": "b742c5bf-3796-4ed8-b563-d5dbb30b7ea1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 111
        },
        {
            "id": "b3b476f9-ecd0-4b91-945f-d6f5a6e69b79",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 115
        },
        {
            "id": "d79df9eb-c0b3-4509-9752-54beec95d85c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 117
        },
        {
            "id": "81457476-434f-4aa1-b411-45c49c08021e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 173
        },
        {
            "id": "038ad48c-30c1-4a5d-9af4-da58013a1024",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 196
        },
        {
            "id": "76a8f147-3168-4318-880a-d35342750266",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 197
        },
        {
            "id": "4b6e29d5-0bde-4dab-bcac-c12eeb2cac70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 229
        },
        {
            "id": "7c19bb61-76f0-44d0-b9b4-b49c44474e65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 46
        },
        {
            "id": "58e96c74-f590-43bd-9c9a-11be68d14e65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 65
        },
        {
            "id": "adaf8af7-6107-4f6a-b5d3-809bc5f3a1c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 196
        },
        {
            "id": "eacd0b24-0484-4a23-8430-6de58ec1b125",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 197
        },
        {
            "id": "3ab0aa6d-cae9-4d99-a2f4-7d81136ffd8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 44
        },
        {
            "id": "307f612f-4887-4e69-ba42-a55fc1d638c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 45
        },
        {
            "id": "f49b4a1b-8736-49ff-a11d-124dd628f5e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 46
        },
        {
            "id": "88a6a053-9f96-4714-9563-b7b37520b771",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 65
        },
        {
            "id": "2e0c0a8b-aee7-4a67-9e5c-75c88e203d81",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 97
        },
        {
            "id": "a6cb2b50-91b7-434d-be8e-589edfce4a04",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 101
        },
        {
            "id": "c8eaaeca-1e69-4941-bedb-5c00221a3856",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 111
        },
        {
            "id": "5c5bb142-eba3-4a6a-a770-2a386a674cbb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 173
        },
        {
            "id": "1f535888-899d-4a8a-96a9-206ebb46c9dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 196
        },
        {
            "id": "fc944646-1fd2-433a-aa80-307604f75b0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 197
        },
        {
            "id": "41049fbf-c810-4839-8b28-8c7dda30f56f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 229
        },
        {
            "id": "f62a6521-95bd-49dc-a7d3-1c04648fbeee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 44
        },
        {
            "id": "f29b305a-1229-4054-bbeb-2700f56cd3ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 46
        },
        {
            "id": "f8689d28-89de-4d4b-b7af-97478db56fd4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 44
        },
        {
            "id": "509b8077-1455-476b-9788-26276c271450",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 45
        },
        {
            "id": "2c4a80e6-13df-452e-8dac-3325dab25e1c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 46
        },
        {
            "id": "7b4228a9-adb8-4157-9270-f8b9a95c28ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 58
        },
        {
            "id": "8abef8d9-b374-479a-bf3e-6f7e4b04fdc7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 59
        },
        {
            "id": "bdd2589b-6054-4dff-bce6-8f00f7e9832b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 65
        },
        {
            "id": "a0f4b565-ad3c-4712-9cb4-269b93c05ae2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 97
        },
        {
            "id": "00732ee8-da51-4848-94cb-5201940a9658",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 100
        },
        {
            "id": "23aa5a70-b40f-4358-b5a2-28621476bbfc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 101
        },
        {
            "id": "553e8738-2057-4de1-a0f6-92e4873cbe37",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 111
        },
        {
            "id": "8d752ca6-3fac-4097-bfcf-583cde862a0c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 112
        },
        {
            "id": "5c466f75-3f2f-4b4e-ade6-34c37a3b37e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 113
        },
        {
            "id": "364d1498-5536-419f-a1f2-ad385cdd3e94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 117
        },
        {
            "id": "9bbb1c6c-93aa-4f45-8c6a-dbe68095581f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 173
        },
        {
            "id": "ca3d2fcd-4915-4e75-a690-ddc7fef83b44",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 196
        },
        {
            "id": "a9815e06-cc12-433e-832c-fb2c98526caa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 197
        },
        {
            "id": "70403287-1d42-4ee1-82bd-0579a789babd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 229
        },
        {
            "id": "a07cb355-79b1-4bc2-b64e-8a38c20ac625",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 44
        },
        {
            "id": "18167cd8-6db4-449a-815e-e49fc0b9683b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 46
        },
        {
            "id": "b5aa2fa6-a791-44b0-9346-040dea47d1c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 46
        },
        {
            "id": "34114519-5f12-46d5-baf1-aa416e274066",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 46
        },
        {
            "id": "941dce6e-5dcf-486f-a41e-57a750298033",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 32
        },
        {
            "id": "1645983e-4713-446e-8276-4ac9e25074ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 46
        },
        {
            "id": "47661c8c-420a-46cc-9d95-5b6e31603765",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 63
        },
        {
            "id": "ebb92d8e-3761-4c85-b2b6-71af6c1787af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 239
        },
        {
            "id": "230d20eb-4b7d-486a-b7f4-8d68f49eafd7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 8221
        },
        {
            "id": "df7f74b7-d473-471b-81b8-3bba8d941223",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 46
        },
        {
            "id": "12787d7d-618f-4928-bb2b-445c16106eca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 46
        },
        {
            "id": "a1f59375-ffcf-4bfb-a99d-8291190700c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 44
        },
        {
            "id": "26dd3e64-0f08-4184-83a6-9d0518549d96",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 46
        },
        {
            "id": "bbbb6088-37c7-4033-a83a-b441db964383",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 46
        },
        {
            "id": "a6cb89f2-ebef-4a6c-8eab-2afdcb371e32",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 44
        },
        {
            "id": "79bd3613-88b8-433f-8190-fb366764be5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 46
        },
        {
            "id": "0634f82d-cfee-4bb6-9f34-158f77b30066",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 44
        },
        {
            "id": "e2d7bc02-20fb-4c30-983c-9b047888cec8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 46
        },
        {
            "id": "b5a0ad03-428c-45f0-8847-ffad238b4747",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 44
        },
        {
            "id": "0fd7b9fd-3b04-48a8-9a25-c2862b5622ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 46
        }
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000a\\u000aDefault Character(9647) ▯",
    "size": 12,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}