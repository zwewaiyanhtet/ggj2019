{
    "id": "b146c4f2-fe98-4361-ad56-234252633807",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSpikeJiggle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 82,
    "bbox_left": 2,
    "bbox_right": 47,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d8e89a92-c203-4b1b-8933-e375e191b718",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b146c4f2-fe98-4361-ad56-234252633807",
            "compositeImage": {
                "id": "a3600524-2c76-4234-8b41-76c1fd5e0996",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d8e89a92-c203-4b1b-8933-e375e191b718",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d28e5a78-1acf-4282-9f9b-d26eb205fa81",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8e89a92-c203-4b1b-8933-e375e191b718",
                    "LayerId": "b68212fb-df76-4eb1-aa7b-3099fd9fc558"
                }
            ]
        },
        {
            "id": "9cdada82-04af-45d3-b69e-2f9fc1352081",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b146c4f2-fe98-4361-ad56-234252633807",
            "compositeImage": {
                "id": "aa13a599-2737-4a72-a532-5586fd04d724",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9cdada82-04af-45d3-b69e-2f9fc1352081",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "56d62707-0bd2-47b4-a73d-1fd13596758d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9cdada82-04af-45d3-b69e-2f9fc1352081",
                    "LayerId": "b68212fb-df76-4eb1-aa7b-3099fd9fc558"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 90,
    "layers": [
        {
            "id": "b68212fb-df76-4eb1-aa7b-3099fd9fc558",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b146c4f2-fe98-4361-ad56-234252633807",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 50,
    "xorig": 0,
    "yorig": 0
}