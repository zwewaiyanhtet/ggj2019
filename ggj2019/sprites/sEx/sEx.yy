{
    "id": "bc8edf82-95c9-4386-964d-edae347aeeac",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sEx",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 62,
    "bbox_left": 19,
    "bbox_right": 44,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3ca9e2d8-c12a-48c7-a0e0-5da11de7754d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bc8edf82-95c9-4386-964d-edae347aeeac",
            "compositeImage": {
                "id": "4506e5f9-57d0-4528-93a1-1bccfaf95657",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3ca9e2d8-c12a-48c7-a0e0-5da11de7754d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "751966e8-2f12-4b40-850a-288b9e25fba7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3ca9e2d8-c12a-48c7-a0e0-5da11de7754d",
                    "LayerId": "a4043b3d-0d96-47fa-95b9-1cfe5af575c8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "a4043b3d-0d96-47fa-95b9-1cfe5af575c8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bc8edf82-95c9-4386-964d-edae347aeeac",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}