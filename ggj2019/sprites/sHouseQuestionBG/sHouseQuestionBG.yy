{
    "id": "c4908f33-88df-4491-b729-9f15e1e17904",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sHouseQuestionBG",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 0,
    "bbox_right": 30,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "30d10bb4-5f3a-43ef-a33c-ed13a93ca0a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4908f33-88df-4491-b729-9f15e1e17904",
            "compositeImage": {
                "id": "a8b3544b-7ab8-4d94-8aaf-32b208dd608e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "30d10bb4-5f3a-43ef-a33c-ed13a93ca0a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "babdc8f6-94be-4773-8ad4-7ff1e7b769ec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "30d10bb4-5f3a-43ef-a33c-ed13a93ca0a0",
                    "LayerId": "67a6f938-570b-401c-bd68-fb171da47ba5"
                }
            ]
        },
        {
            "id": "580f928c-57f6-422d-8f32-aea708c03b7e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4908f33-88df-4491-b729-9f15e1e17904",
            "compositeImage": {
                "id": "686ef86b-3c03-47a6-a998-6f5b247bc01d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "580f928c-57f6-422d-8f32-aea708c03b7e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "283b1e60-963d-4c71-8ad7-9474fd525d32",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "580f928c-57f6-422d-8f32-aea708c03b7e",
                    "LayerId": "67a6f938-570b-401c-bd68-fb171da47ba5"
                }
            ]
        },
        {
            "id": "e187343f-2b2b-4262-954f-e80b913cc3b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4908f33-88df-4491-b729-9f15e1e17904",
            "compositeImage": {
                "id": "4eb2955d-483e-4cbf-8b18-630183276c7a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e187343f-2b2b-4262-954f-e80b913cc3b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f1a3e82-e4cd-4d9a-888e-1980142f8efa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e187343f-2b2b-4262-954f-e80b913cc3b5",
                    "LayerId": "67a6f938-570b-401c-bd68-fb171da47ba5"
                }
            ]
        },
        {
            "id": "eaf3ce80-f281-4fc7-8ff0-48f4c2f811b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4908f33-88df-4491-b729-9f15e1e17904",
            "compositeImage": {
                "id": "ffa198ad-6f84-41ad-94c0-ffa3bfbce587",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eaf3ce80-f281-4fc7-8ff0-48f4c2f811b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "55d79e76-523e-4b34-a08f-35afecd57542",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eaf3ce80-f281-4fc7-8ff0-48f4c2f811b6",
                    "LayerId": "67a6f938-570b-401c-bd68-fb171da47ba5"
                }
            ]
        },
        {
            "id": "e03b6bb0-323d-434d-9bd1-8e35bfcc6c98",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4908f33-88df-4491-b729-9f15e1e17904",
            "compositeImage": {
                "id": "7a235e38-7dca-4c0f-a94b-33a0971c2fa3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e03b6bb0-323d-434d-9bd1-8e35bfcc6c98",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e1fff448-6731-4a04-8d9c-f3f1135bee23",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e03b6bb0-323d-434d-9bd1-8e35bfcc6c98",
                    "LayerId": "67a6f938-570b-401c-bd68-fb171da47ba5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "67a6f938-570b-401c-bd68-fb171da47ba5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c4908f33-88df-4491-b729-9f15e1e17904",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": -108,
    "yorig": 5
}