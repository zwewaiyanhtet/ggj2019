{
    "id": "42f370d4-1f17-4d1c-bcae-1c86e4a626f9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sAxis",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 106,
    "bbox_left": 47,
    "bbox_right": 92,
    "bbox_top": 74,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "16648c27-98cd-46e5-af77-d2195a17ae3b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "42f370d4-1f17-4d1c-bcae-1c86e4a626f9",
            "compositeImage": {
                "id": "c45b1e76-8e36-409e-9138-88a269244560",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "16648c27-98cd-46e5-af77-d2195a17ae3b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "091e0658-5478-4791-88fe-f87e3c216050",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "16648c27-98cd-46e5-af77-d2195a17ae3b",
                    "LayerId": "43246ea1-b90b-44c0-828f-15f094e07fa7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 110,
    "layers": [
        {
            "id": "43246ea1-b90b-44c0-828f-15f094e07fa7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "42f370d4-1f17-4d1c-bcae-1c86e4a626f9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 140,
    "xorig": 69,
    "yorig": 93
}