{
    "id": "08c385e4-cde2-43fa-abfa-1d5f161579df",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sEnemyWings",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 104,
    "bbox_left": 3,
    "bbox_right": 140,
    "bbox_top": 30,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "59da76fe-a35b-45d5-b72d-18d4673edbad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "08c385e4-cde2-43fa-abfa-1d5f161579df",
            "compositeImage": {
                "id": "136dda74-829d-4ae0-8ae1-d1ef01c14d62",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "59da76fe-a35b-45d5-b72d-18d4673edbad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "73dc7a3b-4ce8-4c14-a5c1-daf1a8eac009",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "59da76fe-a35b-45d5-b72d-18d4673edbad",
                    "LayerId": "254186db-b93e-471b-831e-fa0388a6cf58"
                }
            ]
        },
        {
            "id": "ef7c8f23-915c-4db6-a968-4e26c6dc8a96",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "08c385e4-cde2-43fa-abfa-1d5f161579df",
            "compositeImage": {
                "id": "2a080fe2-04ae-4aa9-a7e8-9a9bb8fca2d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef7c8f23-915c-4db6-a968-4e26c6dc8a96",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "83e9697c-6ded-4903-8b05-ded6f4413a97",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef7c8f23-915c-4db6-a968-4e26c6dc8a96",
                    "LayerId": "254186db-b93e-471b-831e-fa0388a6cf58"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 108,
    "layers": [
        {
            "id": "254186db-b93e-471b-831e-fa0388a6cf58",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "08c385e4-cde2-43fa-abfa-1d5f161579df",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 148,
    "xorig": 74,
    "yorig": 54
}