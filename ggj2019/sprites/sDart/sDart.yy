{
    "id": "b566e49d-d6cb-4934-9850-5a516f6246fd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sDart",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d349f0c7-069a-4c03-a9d5-62e4ec3010f3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b566e49d-d6cb-4934-9850-5a516f6246fd",
            "compositeImage": {
                "id": "d39c701d-a271-4ddd-9ce6-bb8aa0364cea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d349f0c7-069a-4c03-a9d5-62e4ec3010f3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e2a841b-3d6a-40bb-b817-bfe06ce30891",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d349f0c7-069a-4c03-a9d5-62e4ec3010f3",
                    "LayerId": "68593572-21fe-49eb-830f-43797fea8286"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "68593572-21fe-49eb-830f-43797fea8286",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b566e49d-d6cb-4934-9850-5a516f6246fd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}