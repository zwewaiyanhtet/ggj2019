{
    "id": "a3d5cd30-e232-4d6b-a352-102871030e86",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sHandle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 100,
    "bbox_left": 6,
    "bbox_right": 137,
    "bbox_top": 13,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "944b4f61-70f7-4142-a65c-49d579919d53",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3d5cd30-e232-4d6b-a352-102871030e86",
            "compositeImage": {
                "id": "647e623e-0cbd-4fc2-94fc-75a7283808e9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "944b4f61-70f7-4142-a65c-49d579919d53",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9dd72888-5a92-49f3-ba2e-5507bf17c76b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "944b4f61-70f7-4142-a65c-49d579919d53",
                    "LayerId": "a9bbe417-e446-4204-8b19-e0129f01ba41"
                }
            ]
        },
        {
            "id": "7aa7b55e-593a-40ec-b5ad-c1cfc448f7af",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3d5cd30-e232-4d6b-a352-102871030e86",
            "compositeImage": {
                "id": "97a2ab4f-21aa-48b4-89f1-17ac85e909d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7aa7b55e-593a-40ec-b5ad-c1cfc448f7af",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "05d47bba-75fc-474b-bd56-f11b5c53341c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7aa7b55e-593a-40ec-b5ad-c1cfc448f7af",
                    "LayerId": "a9bbe417-e446-4204-8b19-e0129f01ba41"
                }
            ]
        },
        {
            "id": "cea424a9-2d5c-4969-9651-ed6b08cd6af4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3d5cd30-e232-4d6b-a352-102871030e86",
            "compositeImage": {
                "id": "3722e25e-f6f1-4776-9242-a865df2f5d00",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cea424a9-2d5c-4969-9651-ed6b08cd6af4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e3048289-6372-4ccd-bc7c-e09f3dac1f75",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cea424a9-2d5c-4969-9651-ed6b08cd6af4",
                    "LayerId": "a9bbe417-e446-4204-8b19-e0129f01ba41"
                }
            ]
        },
        {
            "id": "9d50c901-e9fc-4ec2-b762-1bcda7cf9d7d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3d5cd30-e232-4d6b-a352-102871030e86",
            "compositeImage": {
                "id": "796af5b7-5fe7-4c40-9717-33fa8a64d89e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d50c901-e9fc-4ec2-b762-1bcda7cf9d7d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c83bf0aa-2a18-4b13-b21c-d6163cb55dbb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d50c901-e9fc-4ec2-b762-1bcda7cf9d7d",
                    "LayerId": "a9bbe417-e446-4204-8b19-e0129f01ba41"
                }
            ]
        },
        {
            "id": "8f87de68-c7a4-4938-a244-bea17dcc53e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3d5cd30-e232-4d6b-a352-102871030e86",
            "compositeImage": {
                "id": "261c630a-b969-4f4d-9658-5f5e7835ef9b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f87de68-c7a4-4938-a244-bea17dcc53e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d965c149-36ef-4046-9d27-352c1e7a6b2a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f87de68-c7a4-4938-a244-bea17dcc53e9",
                    "LayerId": "a9bbe417-e446-4204-8b19-e0129f01ba41"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 110,
    "layers": [
        {
            "id": "a9bbe417-e446-4204-8b19-e0129f01ba41",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a3d5cd30-e232-4d6b-a352-102871030e86",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 140,
    "xorig": 72,
    "yorig": 95
}