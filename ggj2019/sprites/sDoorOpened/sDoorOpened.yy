{
    "id": "3784ce46-7049-44bc-ac66-d883935e13ec",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sDoorOpened",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 356,
    "bbox_left": 5,
    "bbox_right": 40,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ce716c5d-b3c5-461b-ba15-1c0e0d1a94f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3784ce46-7049-44bc-ac66-d883935e13ec",
            "compositeImage": {
                "id": "bde709ea-6b34-47a1-acdc-f3c210277e06",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ce716c5d-b3c5-461b-ba15-1c0e0d1a94f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "340d67d9-8164-4f07-9b21-2da2fd865142",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ce716c5d-b3c5-461b-ba15-1c0e0d1a94f9",
                    "LayerId": "7a81a14c-9b4e-4d6a-a9e8-ad59d0b901aa"
                }
            ]
        },
        {
            "id": "877f8a4c-3704-4ec0-aaaf-f12370086d73",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3784ce46-7049-44bc-ac66-d883935e13ec",
            "compositeImage": {
                "id": "d750832c-f4cb-4ed9-b0ee-f10972f7dca8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "877f8a4c-3704-4ec0-aaaf-f12370086d73",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "075c45c7-aed9-4076-9f93-a4f3781c53dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "877f8a4c-3704-4ec0-aaaf-f12370086d73",
                    "LayerId": "7a81a14c-9b4e-4d6a-a9e8-ad59d0b901aa"
                }
            ]
        },
        {
            "id": "60b53dca-54a5-4b38-b6c7-5cc64cfe2a40",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3784ce46-7049-44bc-ac66-d883935e13ec",
            "compositeImage": {
                "id": "6b100551-35d3-424e-a42f-e8e189e60412",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "60b53dca-54a5-4b38-b6c7-5cc64cfe2a40",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a46dc42-a0f4-4c32-b9a4-4190281854e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "60b53dca-54a5-4b38-b6c7-5cc64cfe2a40",
                    "LayerId": "7a81a14c-9b4e-4d6a-a9e8-ad59d0b901aa"
                }
            ]
        },
        {
            "id": "10240d78-e986-443e-85a8-6b3c02d81e16",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3784ce46-7049-44bc-ac66-d883935e13ec",
            "compositeImage": {
                "id": "7b4d1c57-184f-4716-8f00-15dd92cc1a39",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "10240d78-e986-443e-85a8-6b3c02d81e16",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "75fb521c-16b2-48ad-abbb-9266a757543f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "10240d78-e986-443e-85a8-6b3c02d81e16",
                    "LayerId": "7a81a14c-9b4e-4d6a-a9e8-ad59d0b901aa"
                }
            ]
        },
        {
            "id": "8478ea59-69e1-47be-9c10-180fb19ba116",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3784ce46-7049-44bc-ac66-d883935e13ec",
            "compositeImage": {
                "id": "a2040043-b094-4c2b-ba5a-d827eb665c4f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8478ea59-69e1-47be-9c10-180fb19ba116",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df118fc8-5655-445e-ac8b-a0fb8ced9e80",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8478ea59-69e1-47be-9c10-180fb19ba116",
                    "LayerId": "7a81a14c-9b4e-4d6a-a9e8-ad59d0b901aa"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 360,
    "layers": [
        {
            "id": "7a81a14c-9b4e-4d6a-a9e8-ad59d0b901aa",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3784ce46-7049-44bc-ac66-d883935e13ec",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 46,
    "xorig": 23,
    "yorig": 180
}