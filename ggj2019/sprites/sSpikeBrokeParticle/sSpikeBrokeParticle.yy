{
    "id": "0b63dbea-8719-408e-9ecf-15dd46e8c7ab",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSpikeBrokeParticle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 2,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4c32b79d-fbca-4fa0-9bfa-c281aa7d7f9d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0b63dbea-8719-408e-9ecf-15dd46e8c7ab",
            "compositeImage": {
                "id": "24738584-2926-4475-9b0c-4391f292dedc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4c32b79d-fbca-4fa0-9bfa-c281aa7d7f9d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e7340efe-84c6-43e0-af18-5d08c29169e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c32b79d-fbca-4fa0-9bfa-c281aa7d7f9d",
                    "LayerId": "8315fa3a-3bd4-4b89-bd41-95e926f83035"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "8315fa3a-3bd4-4b89-bd41-95e926f83035",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0b63dbea-8719-408e-9ecf-15dd46e8c7ab",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 18,
    "xorig": 9,
    "yorig": 12
}