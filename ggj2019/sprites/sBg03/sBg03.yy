{
    "id": "c1f3d8b5-885e-40de-8c00-3dc70d326d80",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBg03",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "33eba597-569a-4533-afb3-256784e1b4a3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c1f3d8b5-885e-40de-8c00-3dc70d326d80",
            "compositeImage": {
                "id": "b3f8d957-b492-412c-a354-6ec774f1977e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "33eba597-569a-4533-afb3-256784e1b4a3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "836cfef2-3696-45ec-af48-0e84e4da1565",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "33eba597-569a-4533-afb3-256784e1b4a3",
                    "LayerId": "76e83650-9dd8-45bb-a46f-352a846428d0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "76e83650-9dd8-45bb-a46f-352a846428d0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c1f3d8b5-885e-40de-8c00-3dc70d326d80",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}