{
    "id": "035f86e4-57be-4bf9-9f52-04680737bdd3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTrapDoorOpened",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 40,
    "bbox_left": 17,
    "bbox_right": 561,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0575d363-796d-4649-ae34-264fd6e6ef2e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "035f86e4-57be-4bf9-9f52-04680737bdd3",
            "compositeImage": {
                "id": "e483b2cb-ff83-4b16-8c8d-28af9431ad85",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0575d363-796d-4649-ae34-264fd6e6ef2e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "426684ab-01e5-48ad-b24b-d0e2caf56e92",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0575d363-796d-4649-ae34-264fd6e6ef2e",
                    "LayerId": "3ef7bd48-709a-44a5-98be-f88b87e99926"
                }
            ]
        },
        {
            "id": "b08df81d-10ea-4184-9f7b-233fd5c0971e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "035f86e4-57be-4bf9-9f52-04680737bdd3",
            "compositeImage": {
                "id": "3ddb6a34-752c-4eab-9398-15914bdc575c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b08df81d-10ea-4184-9f7b-233fd5c0971e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "75a1bddc-8e00-4801-8a1d-07c4b7cb4cce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b08df81d-10ea-4184-9f7b-233fd5c0971e",
                    "LayerId": "3ef7bd48-709a-44a5-98be-f88b87e99926"
                }
            ]
        },
        {
            "id": "1baee90b-1ca5-4546-8f03-4a0454feb8b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "035f86e4-57be-4bf9-9f52-04680737bdd3",
            "compositeImage": {
                "id": "c073ebd2-9344-4ae4-a9d0-fb8ce845af61",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1baee90b-1ca5-4546-8f03-4a0454feb8b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a2aa5dfa-c477-4c23-88b5-5b37bdade57b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1baee90b-1ca5-4546-8f03-4a0454feb8b7",
                    "LayerId": "3ef7bd48-709a-44a5-98be-f88b87e99926"
                }
            ]
        },
        {
            "id": "0ae761ff-bb13-4bae-aed1-3a3731642705",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "035f86e4-57be-4bf9-9f52-04680737bdd3",
            "compositeImage": {
                "id": "5da19e31-3421-4b53-9534-d9381787f316",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ae761ff-bb13-4bae-aed1-3a3731642705",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "340cf859-5c6f-464c-af1e-2952a80856f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ae761ff-bb13-4bae-aed1-3a3731642705",
                    "LayerId": "3ef7bd48-709a-44a5-98be-f88b87e99926"
                }
            ]
        },
        {
            "id": "a53627ad-422e-41fd-a7b7-cd3d57a080c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "035f86e4-57be-4bf9-9f52-04680737bdd3",
            "compositeImage": {
                "id": "d48828a9-cdc9-4fb7-91a5-b3f166f7723b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a53627ad-422e-41fd-a7b7-cd3d57a080c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3424644d-df83-4e33-a70a-1bbd87479ae8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a53627ad-422e-41fd-a7b7-cd3d57a080c3",
                    "LayerId": "3ef7bd48-709a-44a5-98be-f88b87e99926"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 46,
    "layers": [
        {
            "id": "3ef7bd48-709a-44a5-98be-f88b87e99926",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "035f86e4-57be-4bf9-9f52-04680737bdd3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 576,
    "xorig": 288,
    "yorig": 23
}