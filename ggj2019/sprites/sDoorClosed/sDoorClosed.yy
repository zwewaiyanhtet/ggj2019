{
    "id": "9a8c9dfb-bfa8-4b1e-9fb8-c352285ee031",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sDoorClosed",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 356,
    "bbox_left": 5,
    "bbox_right": 40,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "53d27c6a-75a5-4563-b000-68bce2ef2767",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9a8c9dfb-bfa8-4b1e-9fb8-c352285ee031",
            "compositeImage": {
                "id": "8b47a2b4-d196-4e37-99d8-4f1731c2995c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "53d27c6a-75a5-4563-b000-68bce2ef2767",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef2b93d8-f7af-49a2-a518-96f8167fb77c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "53d27c6a-75a5-4563-b000-68bce2ef2767",
                    "LayerId": "9bd8c969-c4f0-49a0-a7f1-8bd7189d5040"
                }
            ]
        },
        {
            "id": "3a1db814-de1a-4edd-8f45-7535c4f87c70",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9a8c9dfb-bfa8-4b1e-9fb8-c352285ee031",
            "compositeImage": {
                "id": "c965ab96-f61a-4c15-b704-698e3df2bf0b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3a1db814-de1a-4edd-8f45-7535c4f87c70",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ca36c30-7a2c-4f45-9090-6a0dd44eb6d3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3a1db814-de1a-4edd-8f45-7535c4f87c70",
                    "LayerId": "9bd8c969-c4f0-49a0-a7f1-8bd7189d5040"
                }
            ]
        },
        {
            "id": "80a50867-5642-4df0-8f9e-2a7423b85755",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9a8c9dfb-bfa8-4b1e-9fb8-c352285ee031",
            "compositeImage": {
                "id": "a4a900e8-684e-4e84-9897-2505c13ec670",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "80a50867-5642-4df0-8f9e-2a7423b85755",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d361b6dd-b1f2-43ae-830c-f5deef63df76",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "80a50867-5642-4df0-8f9e-2a7423b85755",
                    "LayerId": "9bd8c969-c4f0-49a0-a7f1-8bd7189d5040"
                }
            ]
        },
        {
            "id": "e8bfb209-a444-40eb-bcd5-a7551b14afa7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9a8c9dfb-bfa8-4b1e-9fb8-c352285ee031",
            "compositeImage": {
                "id": "8ae364c2-eb9a-4878-a0a5-d83067c8c00c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e8bfb209-a444-40eb-bcd5-a7551b14afa7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6134977a-369e-4831-a3c1-27a3e1a41ca8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e8bfb209-a444-40eb-bcd5-a7551b14afa7",
                    "LayerId": "9bd8c969-c4f0-49a0-a7f1-8bd7189d5040"
                }
            ]
        },
        {
            "id": "b857463c-ad3f-44b5-9080-b6e2365f5ff0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9a8c9dfb-bfa8-4b1e-9fb8-c352285ee031",
            "compositeImage": {
                "id": "d1e2e70b-4f18-402a-bdeb-11ad7b7a4e31",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b857463c-ad3f-44b5-9080-b6e2365f5ff0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "edf722d9-a316-43d9-b260-393c8423e7cc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b857463c-ad3f-44b5-9080-b6e2365f5ff0",
                    "LayerId": "9bd8c969-c4f0-49a0-a7f1-8bd7189d5040"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 360,
    "layers": [
        {
            "id": "9bd8c969-c4f0-49a0-a7f1-8bd7189d5040",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9a8c9dfb-bfa8-4b1e-9fb8-c352285ee031",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 46,
    "xorig": 23,
    "yorig": 180
}