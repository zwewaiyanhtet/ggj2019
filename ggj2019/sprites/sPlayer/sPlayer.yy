{
    "id": "2a8f440d-9732-4dcc-a4b9-107a365ffbc3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPlayer",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 109,
    "bbox_left": 49,
    "bbox_right": 136,
    "bbox_top": 66,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c5eb6030-ccbf-435a-a03c-43c32e62f7c6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a8f440d-9732-4dcc-a4b9-107a365ffbc3",
            "compositeImage": {
                "id": "8170c346-da8d-4a62-b907-db62b83a893f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c5eb6030-ccbf-435a-a03c-43c32e62f7c6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca7cbc68-92b6-46bd-b05e-0abac0b13cfc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c5eb6030-ccbf-435a-a03c-43c32e62f7c6",
                    "LayerId": "799459f4-4a08-4556-b930-c792e3bc6384"
                }
            ]
        },
        {
            "id": "e665706d-8e81-46ef-9458-53835b0b2e61",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a8f440d-9732-4dcc-a4b9-107a365ffbc3",
            "compositeImage": {
                "id": "8458c1ea-e553-49de-8bdb-7bcc454ea1e1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e665706d-8e81-46ef-9458-53835b0b2e61",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6380ae3f-4f5b-42b9-b81c-65a22ab62921",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e665706d-8e81-46ef-9458-53835b0b2e61",
                    "LayerId": "799459f4-4a08-4556-b930-c792e3bc6384"
                }
            ]
        },
        {
            "id": "b8edb8b9-c165-4c96-bbe5-cd531eed636b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a8f440d-9732-4dcc-a4b9-107a365ffbc3",
            "compositeImage": {
                "id": "72d4aad9-fdf0-4e87-93a9-92d018672dcf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b8edb8b9-c165-4c96-bbe5-cd531eed636b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3380c1b3-fc8a-4755-af84-c3dc4455745c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b8edb8b9-c165-4c96-bbe5-cd531eed636b",
                    "LayerId": "799459f4-4a08-4556-b930-c792e3bc6384"
                }
            ]
        },
        {
            "id": "c6b7cc1e-0535-4cdc-82a4-eddd06757107",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a8f440d-9732-4dcc-a4b9-107a365ffbc3",
            "compositeImage": {
                "id": "367ac7bd-6194-42af-8b15-7977550d707c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c6b7cc1e-0535-4cdc-82a4-eddd06757107",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7ed618de-88c1-4919-92c5-6e46ff9d18f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c6b7cc1e-0535-4cdc-82a4-eddd06757107",
                    "LayerId": "799459f4-4a08-4556-b930-c792e3bc6384"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 110,
    "layers": [
        {
            "id": "799459f4-4a08-4556-b930-c792e3bc6384",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2a8f440d-9732-4dcc-a4b9-107a365ffbc3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 6,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 170,
    "xorig": 85,
    "yorig": 109
}