{
    "id": "24c09a1e-7695-44a3-9388-d6f70b7fbc42",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBg01",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "600586c4-edbb-4d70-a27b-aaef6ac2d134",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "24c09a1e-7695-44a3-9388-d6f70b7fbc42",
            "compositeImage": {
                "id": "151ee6f3-a227-4959-ae13-afe9b546045a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "600586c4-edbb-4d70-a27b-aaef6ac2d134",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "947a4cc9-1b50-44c1-83f2-40ff599824f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "600586c4-edbb-4d70-a27b-aaef6ac2d134",
                    "LayerId": "7abf42ee-044c-4a38-9c61-3347b90264b2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "7abf42ee-044c-4a38-9c61-3347b90264b2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "24c09a1e-7695-44a3-9388-d6f70b7fbc42",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}