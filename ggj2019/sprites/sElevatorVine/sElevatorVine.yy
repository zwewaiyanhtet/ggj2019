{
    "id": "81fb152e-28c3-4ea3-8be6-9fa971a55191",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sElevatorVine",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1535,
    "bbox_left": 3,
    "bbox_right": 24,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e24b32f5-b566-4d76-99f1-0051d006bfed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81fb152e-28c3-4ea3-8be6-9fa971a55191",
            "compositeImage": {
                "id": "93e93575-5837-4aeb-8217-6d64e27ad6eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e24b32f5-b566-4d76-99f1-0051d006bfed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d254dae-3d67-4915-a898-6def2b5d1751",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e24b32f5-b566-4d76-99f1-0051d006bfed",
                    "LayerId": "d0bff2dc-f7d4-4967-868e-b8796d3c27e1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1536,
    "layers": [
        {
            "id": "d0bff2dc-f7d4-4967-868e-b8796d3c27e1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "81fb152e-28c3-4ea3-8be6-9fa971a55191",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 28,
    "xorig": 14,
    "yorig": 1535
}