{
    "id": "3ff184c6-8dfd-489f-b2ba-cbfc562e9fef",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sQuestion",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 61,
    "bbox_left": 12,
    "bbox_right": 51,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "12cb1d01-9a20-433d-8c7f-cab47ccbcaaf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3ff184c6-8dfd-489f-b2ba-cbfc562e9fef",
            "compositeImage": {
                "id": "fd5a293b-4a55-46fe-b574-ed051c522444",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "12cb1d01-9a20-433d-8c7f-cab47ccbcaaf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a10fe834-c34a-41ff-9a6f-ab49464c7dd5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12cb1d01-9a20-433d-8c7f-cab47ccbcaaf",
                    "LayerId": "33674c66-a4be-40d3-8463-336edafec3df"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "33674c66-a4be-40d3-8463-336edafec3df",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3ff184c6-8dfd-489f-b2ba-cbfc562e9fef",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}