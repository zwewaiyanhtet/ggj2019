{
    "id": "0a039868-326e-419a-8963-2c04523c3698",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPlayerDeathParticle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 12,
    "bbox_left": 0,
    "bbox_right": 13,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "48f508da-db3f-4c94-839e-b9a928fc4bdb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0a039868-326e-419a-8963-2c04523c3698",
            "compositeImage": {
                "id": "507dd50b-fc37-4f88-83d2-1fe94029985e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "48f508da-db3f-4c94-839e-b9a928fc4bdb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "735bdaab-4d5c-4885-beea-ff436fcb7aff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "48f508da-db3f-4c94-839e-b9a928fc4bdb",
                    "LayerId": "ca5a5ee1-f659-4271-bdb2-ca1dde381d1a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 14,
    "layers": [
        {
            "id": "ca5a5ee1-f659-4271-bdb2-ca1dde381d1a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0a039868-326e-419a-8963-2c04523c3698",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 14,
    "xorig": 7,
    "yorig": 7
}