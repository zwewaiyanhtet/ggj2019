{
    "id": "b3c1a7a5-bf0f-435a-8dd6-2b756a8da85b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTransitionArea",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ccf57eb6-f238-44af-903b-36bdcc4eab40",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3c1a7a5-bf0f-435a-8dd6-2b756a8da85b",
            "compositeImage": {
                "id": "a37ca6c1-0231-4f4b-8c45-fb0f513854e3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ccf57eb6-f238-44af-903b-36bdcc4eab40",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "68944950-c850-4022-955e-481bba34fa18",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ccf57eb6-f238-44af-903b-36bdcc4eab40",
                    "LayerId": "126341bf-244a-4dd1-a982-a50b19d40a80"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "126341bf-244a-4dd1-a982-a50b19d40a80",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b3c1a7a5-bf0f-435a-8dd6-2b756a8da85b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}