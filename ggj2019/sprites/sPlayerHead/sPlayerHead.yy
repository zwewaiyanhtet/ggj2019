{
    "id": "c9309554-7955-4fd5-8c6e-e8f80ca56bfe",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPlayerHead",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 92,
    "bbox_left": 103,
    "bbox_right": 166,
    "bbox_top": 18,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d447ce13-88d4-4832-922d-3ba6f120771f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c9309554-7955-4fd5-8c6e-e8f80ca56bfe",
            "compositeImage": {
                "id": "df497803-673b-4789-8ffd-f79cbc57644b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d447ce13-88d4-4832-922d-3ba6f120771f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "04458a26-6527-48c6-8566-1ab3abd868b2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d447ce13-88d4-4832-922d-3ba6f120771f",
                    "LayerId": "83cecfd7-dfe1-42f8-acda-c556e3edd096"
                }
            ]
        },
        {
            "id": "f332c60e-2822-4237-aa8d-8c4af796fe12",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c9309554-7955-4fd5-8c6e-e8f80ca56bfe",
            "compositeImage": {
                "id": "153d7a17-477a-48bd-b3c0-1ea65cc5e8d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f332c60e-2822-4237-aa8d-8c4af796fe12",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a99c660-d315-411f-a0c7-d1a9a571b05f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f332c60e-2822-4237-aa8d-8c4af796fe12",
                    "LayerId": "83cecfd7-dfe1-42f8-acda-c556e3edd096"
                }
            ]
        },
        {
            "id": "3663734e-89b1-4fc8-bb8c-9a4c8b7019a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c9309554-7955-4fd5-8c6e-e8f80ca56bfe",
            "compositeImage": {
                "id": "a46ed466-aeb8-4de5-9f72-eb926b0d105a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3663734e-89b1-4fc8-bb8c-9a4c8b7019a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd0ab5f6-4c0d-4035-99f0-718f48e2d742",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3663734e-89b1-4fc8-bb8c-9a4c8b7019a8",
                    "LayerId": "83cecfd7-dfe1-42f8-acda-c556e3edd096"
                }
            ]
        },
        {
            "id": "e4a26cee-3e5f-48ca-a583-a91ddab90886",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c9309554-7955-4fd5-8c6e-e8f80ca56bfe",
            "compositeImage": {
                "id": "62f0daad-dc42-4004-90e3-becec924539e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e4a26cee-3e5f-48ca-a583-a91ddab90886",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f5a234cd-0818-4ef4-a295-092cdd09734d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e4a26cee-3e5f-48ca-a583-a91ddab90886",
                    "LayerId": "83cecfd7-dfe1-42f8-acda-c556e3edd096"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 110,
    "layers": [
        {
            "id": "83cecfd7-dfe1-42f8-acda-c556e3edd096",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c9309554-7955-4fd5-8c6e-e8f80ca56bfe",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 6,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 170,
    "xorig": 85,
    "yorig": 109
}