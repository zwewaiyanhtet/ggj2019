{
    "id": "d85088b8-3318-4193-b6e2-327898438881",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSpike",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 82,
    "bbox_left": 5,
    "bbox_right": 46,
    "bbox_top": 36,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "602ed115-0b23-4dfa-9d19-5d9dcc2f6c33",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d85088b8-3318-4193-b6e2-327898438881",
            "compositeImage": {
                "id": "0f27fdf4-5deb-4ac3-ba89-44d97e3cd059",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "602ed115-0b23-4dfa-9d19-5d9dcc2f6c33",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "88748540-fd20-46cc-8fcf-741476c10281",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "602ed115-0b23-4dfa-9d19-5d9dcc2f6c33",
                    "LayerId": "06ce193d-d855-4c48-a426-88c0ef33094b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 90,
    "layers": [
        {
            "id": "06ce193d-d855-4c48-a426-88c0ef33094b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d85088b8-3318-4193-b6e2-327898438881",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 50,
    "xorig": 0,
    "yorig": 0
}