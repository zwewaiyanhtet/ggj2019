{
    "id": "2275c3ad-0a14-4d5c-9571-32763c833340",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sHandleClose",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 100,
    "bbox_left": 2,
    "bbox_right": 133,
    "bbox_top": 13,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "48779726-f911-4304-8037-a0cc4e2dcf94",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2275c3ad-0a14-4d5c-9571-32763c833340",
            "compositeImage": {
                "id": "487359c0-21c5-474c-963c-e731b28a2c37",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "48779726-f911-4304-8037-a0cc4e2dcf94",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "83443e34-c459-4048-91fa-769187e7df40",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "48779726-f911-4304-8037-a0cc4e2dcf94",
                    "LayerId": "6fef886b-8f8d-4e33-82d8-504c6a1da97c"
                }
            ]
        },
        {
            "id": "e688d940-f111-4f54-962f-925de7945c38",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2275c3ad-0a14-4d5c-9571-32763c833340",
            "compositeImage": {
                "id": "ac010fac-2487-44ff-b751-7b54a7fe34cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e688d940-f111-4f54-962f-925de7945c38",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9178fdcb-a010-4b95-926f-c266ddf43b2b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e688d940-f111-4f54-962f-925de7945c38",
                    "LayerId": "6fef886b-8f8d-4e33-82d8-504c6a1da97c"
                }
            ]
        },
        {
            "id": "bdbdd1de-82fb-4b46-9de0-d8bf23c0ca28",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2275c3ad-0a14-4d5c-9571-32763c833340",
            "compositeImage": {
                "id": "014fd94c-c665-4c6b-8720-46d59c3e7e22",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bdbdd1de-82fb-4b46-9de0-d8bf23c0ca28",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fe65a32b-131c-437c-a234-a0866dbafd26",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bdbdd1de-82fb-4b46-9de0-d8bf23c0ca28",
                    "LayerId": "6fef886b-8f8d-4e33-82d8-504c6a1da97c"
                }
            ]
        },
        {
            "id": "40607293-a38c-46ea-b407-882269335ee6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2275c3ad-0a14-4d5c-9571-32763c833340",
            "compositeImage": {
                "id": "c087bca3-57cf-471d-b7fe-8d1a368cf581",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "40607293-a38c-46ea-b407-882269335ee6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e09dfdbe-1de1-4210-b0df-eef2a60913be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "40607293-a38c-46ea-b407-882269335ee6",
                    "LayerId": "6fef886b-8f8d-4e33-82d8-504c6a1da97c"
                }
            ]
        },
        {
            "id": "8c85b0f8-16f0-4134-921e-39cf5697489b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2275c3ad-0a14-4d5c-9571-32763c833340",
            "compositeImage": {
                "id": "1e6577a7-e9c3-4a86-b80c-b870d5de3b65",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c85b0f8-16f0-4134-921e-39cf5697489b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f3405f29-fbd6-4446-af62-e5879d19ce34",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c85b0f8-16f0-4134-921e-39cf5697489b",
                    "LayerId": "6fef886b-8f8d-4e33-82d8-504c6a1da97c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 110,
    "layers": [
        {
            "id": "6fef886b-8f8d-4e33-82d8-504c6a1da97c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2275c3ad-0a14-4d5c-9571-32763c833340",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 140,
    "xorig": 68,
    "yorig": 96
}