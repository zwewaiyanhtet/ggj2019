{
    "id": "b0514e2a-a956-4dfe-8742-854e89813176",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sHouseQuestion",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 58,
    "bbox_left": 1,
    "bbox_right": 62,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "785cb977-7b19-4d05-b8ab-87d5a348001c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0514e2a-a956-4dfe-8742-854e89813176",
            "compositeImage": {
                "id": "8e1f7cfd-b219-42cd-9725-82c8398788f4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "785cb977-7b19-4d05-b8ab-87d5a348001c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c17290e-6a2a-48ce-93e7-bf0b982d7965",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "785cb977-7b19-4d05-b8ab-87d5a348001c",
                    "LayerId": "d912e827-6e31-4fcd-bca9-a287774a4fea"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "d912e827-6e31-4fcd-bca9-a287774a4fea",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b0514e2a-a956-4dfe-8742-854e89813176",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}