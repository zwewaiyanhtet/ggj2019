{
    "id": "a7c6f7c1-4ddb-4bc6-abc7-896f6c35d9bb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sShell",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 15,
    "bbox_right": 124,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d0bd632d-9666-4212-8258-b0bc9a201dad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7c6f7c1-4ddb-4bc6-abc7-896f6c35d9bb",
            "compositeImage": {
                "id": "00ff17a6-fa88-4989-9bd0-7fef26262bab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d0bd632d-9666-4212-8258-b0bc9a201dad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ebdcc4d4-908e-4071-b25b-bbdb431e143b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d0bd632d-9666-4212-8258-b0bc9a201dad",
                    "LayerId": "e23dacbe-061a-4e7e-9034-02e766b5d499"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 110,
    "layers": [
        {
            "id": "e23dacbe-061a-4e7e-9034-02e766b5d499",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a7c6f7c1-4ddb-4bc6-abc7-896f6c35d9bb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 170,
    "xorig": 85,
    "yorig": 109
}