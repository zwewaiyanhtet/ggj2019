{
    "id": "7b0a0de6-f3a0-4031-b98f-bd8153ef3459",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBubbleBG",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 61,
    "bbox_left": 0,
    "bbox_right": 62,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "17bfd2b8-e4d4-4fa2-831e-40e26acd1991",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7b0a0de6-f3a0-4031-b98f-bd8153ef3459",
            "compositeImage": {
                "id": "cbe67e72-3a16-48be-b674-5a6ba92b6cd2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "17bfd2b8-e4d4-4fa2-831e-40e26acd1991",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62decf61-f7fe-41cf-86b0-9a637bea7dcc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "17bfd2b8-e4d4-4fa2-831e-40e26acd1991",
                    "LayerId": "7eb09d9b-7afe-49d1-a552-2e94cb4e953a"
                }
            ]
        },
        {
            "id": "d0e233b9-3c16-4af4-b58f-a60b516dd28a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7b0a0de6-f3a0-4031-b98f-bd8153ef3459",
            "compositeImage": {
                "id": "6eddd97b-bb9c-4940-ba27-6b7249975933",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d0e233b9-3c16-4af4-b58f-a60b516dd28a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cb6d6714-6406-4f47-855f-b7f4759662f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d0e233b9-3c16-4af4-b58f-a60b516dd28a",
                    "LayerId": "7eb09d9b-7afe-49d1-a552-2e94cb4e953a"
                }
            ]
        },
        {
            "id": "24ea03ed-7431-4db7-b661-36cef0998472",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7b0a0de6-f3a0-4031-b98f-bd8153ef3459",
            "compositeImage": {
                "id": "77d08998-eb0f-40a2-a917-da0c01351082",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "24ea03ed-7431-4db7-b661-36cef0998472",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "213ef0c1-32f2-42a1-b027-4954d5c4b538",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "24ea03ed-7431-4db7-b661-36cef0998472",
                    "LayerId": "7eb09d9b-7afe-49d1-a552-2e94cb4e953a"
                }
            ]
        },
        {
            "id": "4b6aa0cf-7412-4bd7-9672-dfa2e361ccf2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7b0a0de6-f3a0-4031-b98f-bd8153ef3459",
            "compositeImage": {
                "id": "350fa116-9c10-4a2e-a670-5ca0ad5af653",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4b6aa0cf-7412-4bd7-9672-dfa2e361ccf2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "076e5ffb-1d7a-4842-acf3-6a99f6ec9e67",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4b6aa0cf-7412-4bd7-9672-dfa2e361ccf2",
                    "LayerId": "7eb09d9b-7afe-49d1-a552-2e94cb4e953a"
                }
            ]
        },
        {
            "id": "233dd8fe-6579-4cd6-9504-d3ead65ca11d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7b0a0de6-f3a0-4031-b98f-bd8153ef3459",
            "compositeImage": {
                "id": "8c88701c-581d-41f6-ae3d-074b2e11b70d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "233dd8fe-6579-4cd6-9504-d3ead65ca11d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee98ff7e-503d-4b6d-89c8-b23f54e7a759",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "233dd8fe-6579-4cd6-9504-d3ead65ca11d",
                    "LayerId": "7eb09d9b-7afe-49d1-a552-2e94cb4e953a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "7eb09d9b-7afe-49d1-a552-2e94cb4e953a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7b0a0de6-f3a0-4031-b98f-bd8153ef3459",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": -194,
    "yorig": 7
}