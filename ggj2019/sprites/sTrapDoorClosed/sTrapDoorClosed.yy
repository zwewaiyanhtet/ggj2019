{
    "id": "d41f7ac3-5536-4779-a695-f024608fb2a9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTrapDoorClosed",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 40,
    "bbox_left": 17,
    "bbox_right": 561,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7b814843-1ce8-4824-8730-a54c344cea5e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d41f7ac3-5536-4779-a695-f024608fb2a9",
            "compositeImage": {
                "id": "846741ef-d2fe-43f5-ade9-98c09c6c626d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b814843-1ce8-4824-8730-a54c344cea5e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7dad6cbf-14b9-4102-aaab-d7031f19836e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b814843-1ce8-4824-8730-a54c344cea5e",
                    "LayerId": "fba5b101-87e1-403d-8f43-8308bf10ce21"
                }
            ]
        },
        {
            "id": "8233b0e1-8ede-4804-a10b-52b70998fefb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d41f7ac3-5536-4779-a695-f024608fb2a9",
            "compositeImage": {
                "id": "3b029ca1-9032-4af0-b621-bf7403a33ffb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8233b0e1-8ede-4804-a10b-52b70998fefb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d1e1ed63-a22c-4f91-9ee1-c1c2258d4061",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8233b0e1-8ede-4804-a10b-52b70998fefb",
                    "LayerId": "fba5b101-87e1-403d-8f43-8308bf10ce21"
                }
            ]
        },
        {
            "id": "40f54a3f-cb51-4d95-b420-cde3c7a2f597",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d41f7ac3-5536-4779-a695-f024608fb2a9",
            "compositeImage": {
                "id": "e0a50b7c-4c82-4930-9f21-86f85044b0f6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "40f54a3f-cb51-4d95-b420-cde3c7a2f597",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3ff51e3b-b56c-466d-a743-63ff12550b1b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "40f54a3f-cb51-4d95-b420-cde3c7a2f597",
                    "LayerId": "fba5b101-87e1-403d-8f43-8308bf10ce21"
                }
            ]
        },
        {
            "id": "48bd734e-76d0-471e-a3c8-c61bf0e855db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d41f7ac3-5536-4779-a695-f024608fb2a9",
            "compositeImage": {
                "id": "575fb578-86b5-4d34-a3be-9637b901d85b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "48bd734e-76d0-471e-a3c8-c61bf0e855db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca7e8364-ac8d-44f1-8035-ef5f5de7a1dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "48bd734e-76d0-471e-a3c8-c61bf0e855db",
                    "LayerId": "fba5b101-87e1-403d-8f43-8308bf10ce21"
                }
            ]
        },
        {
            "id": "186b2b6b-510f-4287-ae1b-cfde687a1a20",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d41f7ac3-5536-4779-a695-f024608fb2a9",
            "compositeImage": {
                "id": "f8ce674e-7aa8-4691-a77c-f39b05840789",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "186b2b6b-510f-4287-ae1b-cfde687a1a20",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2fb2dc6a-016a-4dbe-abca-ada6010a9ac9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "186b2b6b-510f-4287-ae1b-cfde687a1a20",
                    "LayerId": "fba5b101-87e1-403d-8f43-8308bf10ce21"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 46,
    "layers": [
        {
            "id": "fba5b101-87e1-403d-8f43-8308bf10ce21",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d41f7ac3-5536-4779-a695-f024608fb2a9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 576,
    "xorig": 288,
    "yorig": 23
}