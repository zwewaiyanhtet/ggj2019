{
    "id": "fe00a8d5-7064-4ba9-ac81-0bbc50c456ea",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sGround",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "43626016-2924-4d4a-933f-ca111407c74b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fe00a8d5-7064-4ba9-ac81-0bbc50c456ea",
            "compositeImage": {
                "id": "c0d53f87-34f4-45fb-addb-9dabaa17961e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "43626016-2924-4d4a-933f-ca111407c74b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e199852b-cd22-483d-bcfa-4212a5fb4edf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43626016-2924-4d4a-933f-ca111407c74b",
                    "LayerId": "96212b9c-8fd0-42f3-bc1e-d7b7593156c8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "96212b9c-8fd0-42f3-bc1e-d7b7593156c8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fe00a8d5-7064-4ba9-ac81-0bbc50c456ea",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}