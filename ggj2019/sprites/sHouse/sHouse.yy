{
    "id": "de19f2fa-9f40-43d3-918f-fe1b2525e0ca",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sHouse",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 58,
    "bbox_left": 1,
    "bbox_right": 62,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6156a838-9535-4b29-95d4-f1ce8a0e6dd2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "de19f2fa-9f40-43d3-918f-fe1b2525e0ca",
            "compositeImage": {
                "id": "e71c238d-4e0d-4d43-9047-9fcbe5a72cab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6156a838-9535-4b29-95d4-f1ce8a0e6dd2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "287a31af-16b4-4359-ba89-5794a69a02c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6156a838-9535-4b29-95d4-f1ce8a0e6dd2",
                    "LayerId": "9661d6c6-4d11-44d2-9cb4-e756886bce83"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "9661d6c6-4d11-44d2-9cb4-e756886bce83",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "de19f2fa-9f40-43d3-918f-fe1b2525e0ca",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}