{
    "id": "42ca2bbe-cd23-43cd-ab7d-c38ed97665c7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sGrass",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9fbfee5e-0b3a-4ebf-9176-acb86ff80daf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "42ca2bbe-cd23-43cd-ab7d-c38ed97665c7",
            "compositeImage": {
                "id": "d5f17cc6-373a-4435-9bf2-56e83bb27e6c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9fbfee5e-0b3a-4ebf-9176-acb86ff80daf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7818f2b3-cfc0-400b-a5e5-f4205350a4d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9fbfee5e-0b3a-4ebf-9176-acb86ff80daf",
                    "LayerId": "7bbfefd8-0fa3-4b16-8c5f-8f00cae9deca"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "7bbfefd8-0fa3-4b16-8c5f-8f00cae9deca",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "42ca2bbe-cd23-43cd-ab7d-c38ed97665c7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}