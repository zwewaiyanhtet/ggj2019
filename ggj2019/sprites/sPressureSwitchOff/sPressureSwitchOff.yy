{
    "id": "00e33c9f-7fe7-4e50-b971-2ae1de21cca9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPressureSwitchOff",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 62,
    "bbox_left": 0,
    "bbox_right": 159,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "decfa561-6e72-4e06-85ec-7edeb9332176",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "00e33c9f-7fe7-4e50-b971-2ae1de21cca9",
            "compositeImage": {
                "id": "b3f3b34b-3d2b-43bc-9e8c-0c1afa70831e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "decfa561-6e72-4e06-85ec-7edeb9332176",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e494930-802f-43bd-a94e-25875df96675",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "decfa561-6e72-4e06-85ec-7edeb9332176",
                    "LayerId": "99b5ad08-3a86-4577-a135-b49437651df3"
                }
            ]
        },
        {
            "id": "30d8aa2e-9ffd-4d14-ad1e-cb82c87a998f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "00e33c9f-7fe7-4e50-b971-2ae1de21cca9",
            "compositeImage": {
                "id": "8c08869d-dba2-4e5e-8df9-94a352419569",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "30d8aa2e-9ffd-4d14-ad1e-cb82c87a998f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "463ca1c7-3bfc-4712-9c80-4320fa35727a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "30d8aa2e-9ffd-4d14-ad1e-cb82c87a998f",
                    "LayerId": "99b5ad08-3a86-4577-a135-b49437651df3"
                }
            ]
        },
        {
            "id": "58e6f153-0c6d-4038-9bd3-1438ef404326",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "00e33c9f-7fe7-4e50-b971-2ae1de21cca9",
            "compositeImage": {
                "id": "25de53c8-2144-4488-a6e4-fcb6c1d44599",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "58e6f153-0c6d-4038-9bd3-1438ef404326",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "644fef59-d49a-4cac-a402-f57b6480c672",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "58e6f153-0c6d-4038-9bd3-1438ef404326",
                    "LayerId": "99b5ad08-3a86-4577-a135-b49437651df3"
                }
            ]
        },
        {
            "id": "e003c581-0bcd-46ac-a6ec-1a985c95476f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "00e33c9f-7fe7-4e50-b971-2ae1de21cca9",
            "compositeImage": {
                "id": "adfc6349-eb6a-4bfc-9b5f-6e3c0a2dc94b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e003c581-0bcd-46ac-a6ec-1a985c95476f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4449f402-7e7b-40d5-9b7e-5615a02991b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e003c581-0bcd-46ac-a6ec-1a985c95476f",
                    "LayerId": "99b5ad08-3a86-4577-a135-b49437651df3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 70,
    "layers": [
        {
            "id": "99b5ad08-3a86-4577-a135-b49437651df3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "00e33c9f-7fe7-4e50-b971-2ae1de21cca9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 20,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 160,
    "xorig": 80,
    "yorig": 35
}