{
    "id": "c9bf2788-959f-470c-9a9e-1e311e56dd07",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sEnemy",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 81,
    "bbox_left": 53,
    "bbox_right": 112,
    "bbox_top": 26,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a2f95730-88b3-49bf-aea5-d0a29f71a58a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c9bf2788-959f-470c-9a9e-1e311e56dd07",
            "compositeImage": {
                "id": "944cd5bd-582e-4b21-ac2b-323f3a170c14",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a2f95730-88b3-49bf-aea5-d0a29f71a58a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "573fbfbf-9531-4053-bede-b49f30244bec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a2f95730-88b3-49bf-aea5-d0a29f71a58a",
                    "LayerId": "9ec4a6b1-cdb3-4267-adf5-ecd6cd9816be"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 108,
    "layers": [
        {
            "id": "9ec4a6b1-cdb3-4267-adf5-ecd6cd9816be",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c9bf2788-959f-470c-9a9e-1e311e56dd07",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 148,
    "xorig": 74,
    "yorig": 54
}