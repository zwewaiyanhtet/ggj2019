{
    "id": "cc5da4ed-7c1f-42ad-9ee1-e0032d587d96",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sElevator",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 44,
    "bbox_left": 2,
    "bbox_right": 237,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "66f9c5ae-6fb2-40bb-97e5-de18be95ec6d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cc5da4ed-7c1f-42ad-9ee1-e0032d587d96",
            "compositeImage": {
                "id": "7c4a6658-dc91-4062-b32e-c986665e2555",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "66f9c5ae-6fb2-40bb-97e5-de18be95ec6d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f02fbef-3fd4-434f-80dd-81a27ad5ce48",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "66f9c5ae-6fb2-40bb-97e5-de18be95ec6d",
                    "LayerId": "d25f53e4-e548-4c61-89c8-834b1b8272c8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 50,
    "layers": [
        {
            "id": "d25f53e4-e548-4c61-89c8-834b1b8272c8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cc5da4ed-7c1f-42ad-9ee1-e0032d587d96",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 240,
    "xorig": 120,
    "yorig": 25
}