{
    "id": "fabab2be-f10d-42af-b135-6e39387af355",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPressureSwitch",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 62,
    "bbox_left": 0,
    "bbox_right": 159,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7bb632eb-3ea3-46a9-956a-90218c5ffc70",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fabab2be-f10d-42af-b135-6e39387af355",
            "compositeImage": {
                "id": "a18711a0-af95-4a04-81e0-5f1e8fbf6ec5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7bb632eb-3ea3-46a9-956a-90218c5ffc70",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "89b6ea25-1fbb-47e6-8dc7-7079e95fd661",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7bb632eb-3ea3-46a9-956a-90218c5ffc70",
                    "LayerId": "e3b7c795-893f-45c6-944b-7d4c110a3acc"
                }
            ]
        },
        {
            "id": "1bf8f1e2-25c9-48c7-96ae-004011d9737c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fabab2be-f10d-42af-b135-6e39387af355",
            "compositeImage": {
                "id": "5bb4bbd2-e864-4576-a7f2-2d958b91dcff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1bf8f1e2-25c9-48c7-96ae-004011d9737c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "666a4555-eed2-46cc-8817-2fece5c0c843",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1bf8f1e2-25c9-48c7-96ae-004011d9737c",
                    "LayerId": "e3b7c795-893f-45c6-944b-7d4c110a3acc"
                }
            ]
        },
        {
            "id": "f8937f67-8a20-4e16-9260-248ffa02eb96",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fabab2be-f10d-42af-b135-6e39387af355",
            "compositeImage": {
                "id": "8a845925-0f56-47cd-afca-cae33e2aaa5b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f8937f67-8a20-4e16-9260-248ffa02eb96",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b4d98ceb-10ad-49bb-b8de-50e1878c3794",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f8937f67-8a20-4e16-9260-248ffa02eb96",
                    "LayerId": "e3b7c795-893f-45c6-944b-7d4c110a3acc"
                }
            ]
        },
        {
            "id": "c9a4e797-cc55-4675-895e-356b16693c0a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fabab2be-f10d-42af-b135-6e39387af355",
            "compositeImage": {
                "id": "5d710298-e8b4-4f0f-aa01-57891c11085c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c9a4e797-cc55-4675-895e-356b16693c0a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "35deb86e-779f-45fe-a7ba-30ced1aa078e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c9a4e797-cc55-4675-895e-356b16693c0a",
                    "LayerId": "e3b7c795-893f-45c6-944b-7d4c110a3acc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 70,
    "layers": [
        {
            "id": "e3b7c795-893f-45c6-944b-7d4c110a3acc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fabab2be-f10d-42af-b135-6e39387af355",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 20,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 160,
    "xorig": 80,
    "yorig": 35
}