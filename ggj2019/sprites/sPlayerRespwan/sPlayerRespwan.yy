{
    "id": "2fc5cd41-27fc-4498-9ba3-0c29c20e180e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPlayerRespwan",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 109,
    "bbox_left": 14,
    "bbox_right": 169,
    "bbox_top": 23,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d1eaece7-6ab9-4ebb-8d74-483e1a04b4f8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2fc5cd41-27fc-4498-9ba3-0c29c20e180e",
            "compositeImage": {
                "id": "04b8c10d-cb9d-42cb-b503-191c5be92ece",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d1eaece7-6ab9-4ebb-8d74-483e1a04b4f8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "88e64b67-6965-4602-800f-09058eddf1c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d1eaece7-6ab9-4ebb-8d74-483e1a04b4f8",
                    "LayerId": "dd670e5b-ff1f-40aa-aacc-d01a8a30311b"
                }
            ]
        },
        {
            "id": "0cba5360-49d0-4000-8d8b-67c5c53c7f3d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2fc5cd41-27fc-4498-9ba3-0c29c20e180e",
            "compositeImage": {
                "id": "e0f32c10-3d44-4b9c-a94b-addc4eab8db0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0cba5360-49d0-4000-8d8b-67c5c53c7f3d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c6f84b49-f840-4600-b63c-9bcd28b199e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0cba5360-49d0-4000-8d8b-67c5c53c7f3d",
                    "LayerId": "dd670e5b-ff1f-40aa-aacc-d01a8a30311b"
                }
            ]
        },
        {
            "id": "51702ace-3c5d-4802-91bc-3dbd9404efcb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2fc5cd41-27fc-4498-9ba3-0c29c20e180e",
            "compositeImage": {
                "id": "50ea786d-e313-4139-b5e5-0ed5ed37eeaa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "51702ace-3c5d-4802-91bc-3dbd9404efcb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b826b8b2-bf28-4145-8762-ecf53930c5bf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51702ace-3c5d-4802-91bc-3dbd9404efcb",
                    "LayerId": "dd670e5b-ff1f-40aa-aacc-d01a8a30311b"
                }
            ]
        },
        {
            "id": "9d632859-82c6-433c-8add-8c17da09941e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2fc5cd41-27fc-4498-9ba3-0c29c20e180e",
            "compositeImage": {
                "id": "ff863787-3417-4c68-86db-c36a7a86eb3f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d632859-82c6-433c-8add-8c17da09941e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fbfabbbe-9b68-4cf9-a016-35fe65a9f219",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d632859-82c6-433c-8add-8c17da09941e",
                    "LayerId": "dd670e5b-ff1f-40aa-aacc-d01a8a30311b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 110,
    "layers": [
        {
            "id": "dd670e5b-ff1f-40aa-aacc-d01a8a30311b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2fc5cd41-27fc-4498-9ba3-0c29c20e180e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 170,
    "xorig": 85,
    "yorig": 109
}