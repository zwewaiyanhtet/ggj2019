{
    "id": "214bbba5-c702-4fba-a91d-c45317a29ada",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBg02",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "44af6706-6b75-4a51-bfd2-a20a6b2e5228",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "214bbba5-c702-4fba-a91d-c45317a29ada",
            "compositeImage": {
                "id": "e2d874b3-4d49-47d4-bcf3-ab729e412f41",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "44af6706-6b75-4a51-bfd2-a20a6b2e5228",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c22c7854-e212-4b91-9f5e-afa6ec7b4cf0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "44af6706-6b75-4a51-bfd2-a20a6b2e5228",
                    "LayerId": "257ffc9c-e3bd-4ce9-905c-e2c3d062479f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "257ffc9c-e3bd-4ce9-905c-e2c3d062479f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "214bbba5-c702-4fba-a91d-c45317a29ada",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}