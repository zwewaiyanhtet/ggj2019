{
    "id": "4f68c656-5d98-4a68-b400-8d180ae66d80",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sShellCollision",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 33,
    "bbox_left": 30,
    "bbox_right": 33,
    "bbox_top": 30,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c2771d24-85a7-4b28-8bff-ce08ca427b97",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f68c656-5d98-4a68-b400-8d180ae66d80",
            "compositeImage": {
                "id": "8ece41fc-4309-4f39-993d-6f9c8e877ac4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c2771d24-85a7-4b28-8bff-ce08ca427b97",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "22007a6e-37b0-4d07-8e82-b4746e51e072",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c2771d24-85a7-4b28-8bff-ce08ca427b97",
                    "LayerId": "bb7e71f4-0cb6-4c6c-9998-843397d7227c"
                }
            ]
        }
    ],
    "gridX": 32,
    "gridY": 32,
    "height": 64,
    "layers": [
        {
            "id": "bb7e71f4-0cb6-4c6c-9998-843397d7227c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4f68c656-5d98-4a68-b400-8d180ae66d80",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}