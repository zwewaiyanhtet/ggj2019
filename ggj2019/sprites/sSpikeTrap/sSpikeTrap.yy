{
    "id": "311b649e-d275-459b-bc0e-96f6df7b47ed",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSpikeTrap",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 2,
    "bbox_right": 61,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9438c4f0-fe11-4cea-becc-05e7ede486c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "311b649e-d275-459b-bc0e-96f6df7b47ed",
            "compositeImage": {
                "id": "1b5a87c4-fabe-4184-8493-a92515856d8d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9438c4f0-fe11-4cea-becc-05e7ede486c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8dae02cd-29dc-49ee-aee8-8286faf836d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9438c4f0-fe11-4cea-becc-05e7ede486c2",
                    "LayerId": "1cf166d2-244f-41a4-a370-a1c5f9076416"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "1cf166d2-244f-41a4-a370-a1c5f9076416",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "311b649e-d275-459b-bc0e-96f6df7b47ed",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}