{
    "id": "3400e309-bb03-49ca-a1fa-696e878d24ab",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sMarker",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 8,
    "bbox_left": 0,
    "bbox_right": 16,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a17b89cb-f779-4019-a877-23ff8a5db6cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3400e309-bb03-49ca-a1fa-696e878d24ab",
            "compositeImage": {
                "id": "19c93dcc-2ca9-4db5-b606-dcf8cb5fa868",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a17b89cb-f779-4019-a877-23ff8a5db6cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6341027c-dbcf-4f33-9ad7-61af293cc662",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a17b89cb-f779-4019-a877-23ff8a5db6cf",
                    "LayerId": "62497911-17f7-47fa-b399-be12aa7408fe"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 9,
    "layers": [
        {
            "id": "62497911-17f7-47fa-b399-be12aa7408fe",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3400e309-bb03-49ca-a1fa-696e878d24ab",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 17,
    "xorig": 8,
    "yorig": -1
}