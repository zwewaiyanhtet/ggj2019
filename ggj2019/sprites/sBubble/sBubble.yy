{
    "id": "f516dfe9-4c12-4cef-b701-8442de3ac5d5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBubble",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 77,
    "bbox_left": 0,
    "bbox_right": 78,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "42c1a86b-1c73-48a3-b470-4351fa9a2ff0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f516dfe9-4c12-4cef-b701-8442de3ac5d5",
            "compositeImage": {
                "id": "531dfed0-01ce-437e-ad40-bd82f3e0c582",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "42c1a86b-1c73-48a3-b470-4351fa9a2ff0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7d414895-71f9-41f5-a7bd-ceeffe351155",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "42c1a86b-1c73-48a3-b470-4351fa9a2ff0",
                    "LayerId": "fb7bb213-dd6d-43d4-992f-9d22480bc708"
                }
            ]
        },
        {
            "id": "e9cc3e46-7244-4793-9e20-d79cee60086c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f516dfe9-4c12-4cef-b701-8442de3ac5d5",
            "compositeImage": {
                "id": "c83927d7-aad1-42ea-bfa4-65bd2f36df6a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9cc3e46-7244-4793-9e20-d79cee60086c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ddefa3ac-8813-468a-b9f4-e51f596673c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9cc3e46-7244-4793-9e20-d79cee60086c",
                    "LayerId": "fb7bb213-dd6d-43d4-992f-9d22480bc708"
                }
            ]
        },
        {
            "id": "1d462b0c-dbfb-41df-b177-a3d222ad3334",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f516dfe9-4c12-4cef-b701-8442de3ac5d5",
            "compositeImage": {
                "id": "682b3d39-0695-444c-b2df-ef940da6c1ea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1d462b0c-dbfb-41df-b177-a3d222ad3334",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "faa97d39-c618-4352-a983-8d8495957f88",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d462b0c-dbfb-41df-b177-a3d222ad3334",
                    "LayerId": "fb7bb213-dd6d-43d4-992f-9d22480bc708"
                }
            ]
        },
        {
            "id": "757a56bc-48e1-40cc-98ff-8bd1f7afbdd1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f516dfe9-4c12-4cef-b701-8442de3ac5d5",
            "compositeImage": {
                "id": "e3c3506f-6129-413f-9823-b1415ed13e0b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "757a56bc-48e1-40cc-98ff-8bd1f7afbdd1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "13b74bc0-a604-441e-b209-34f34627a70c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "757a56bc-48e1-40cc-98ff-8bd1f7afbdd1",
                    "LayerId": "fb7bb213-dd6d-43d4-992f-9d22480bc708"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 80,
    "layers": [
        {
            "id": "fb7bb213-dd6d-43d4-992f-9d22480bc708",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f516dfe9-4c12-4cef-b701-8442de3ac5d5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 80,
    "xorig": 42,
    "yorig": 31
}