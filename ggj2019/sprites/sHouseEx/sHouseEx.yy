{
    "id": "2120c1a1-af04-4c9f-8fdb-97e54aa7d2bd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sHouseEx",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 58,
    "bbox_left": 1,
    "bbox_right": 62,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "dc3f55c2-fbab-4983-9148-9043add8c6c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2120c1a1-af04-4c9f-8fdb-97e54aa7d2bd",
            "compositeImage": {
                "id": "be846c6d-99a4-4145-b03f-0391310c5550",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dc3f55c2-fbab-4983-9148-9043add8c6c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d0618ef7-caae-49ff-8bdd-da11d37f05a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dc3f55c2-fbab-4983-9148-9043add8c6c3",
                    "LayerId": "98e2ab6a-ad17-4d32-952b-f4881c38d5b6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "98e2ab6a-ad17-4d32-952b-f4881c38d5b6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2120c1a1-af04-4c9f-8fdb-97e54aa7d2bd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}