{
    "id": "adf7274c-c954-48f9-98bb-31312c29f422",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oAxis",
    "eventList": [
        {
            "id": "19ebe469-2d80-4c67-9384-4474843ee2d5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "adf7274c-c954-48f9-98bb-31312c29f422"
        },
        {
            "id": "57227190-4345-43b1-be00-5139a22fbe6f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "adf7274c-c954-48f9-98bb-31312c29f422"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "42f370d4-1f17-4d1c-bcae-1c86e4a626f9",
    "visible": true
}