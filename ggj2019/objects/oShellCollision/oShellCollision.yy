{
    "id": "41d402f7-fad3-40c7-8b6c-d11b68de6fb2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oShellCollision",
    "eventList": [
        {
            "id": "02995621-5678-4fdd-a828-495d26eb5e85",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "41d402f7-fad3-40c7-8b6c-d11b68de6fb2"
        },
        {
            "id": "60d6b1df-9115-4e92-948d-c96d8d6a3ad5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "41d402f7-fad3-40c7-8b6c-d11b68de6fb2"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "4f68c656-5d98-4a68-b400-8d180ae66d80",
    "visible": false
}