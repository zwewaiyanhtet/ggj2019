if(actor.carrier != noone) {
	if(actor.isBeingCarried){
		if(place_meeting(x, y, oGround)) {
			actor.carrier.movable = false;
		} else {
			actor.carrier.movable = true;	
		}
	} else {
		actor.carrier.movable = true;	
	}
}
