{
    "id": "52bdee9f-5cd2-4044-bbe5-79e67949a89c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oEnemyWings",
    "eventList": [
        {
            "id": "1d4c22cb-83ca-4b46-8712-6cf79a9f8e3d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "52bdee9f-5cd2-4044-bbe5-79e67949a89c"
        },
        {
            "id": "e867a775-37ae-4eb7-8ea2-735928f56b5e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "52bdee9f-5cd2-4044-bbe5-79e67949a89c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "08c385e4-cde2-43fa-abfa-1d5f161579df",
    "visible": true
}