{
    "id": "4b2ed59f-e3ec-4367-a774-cae77ce7815b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oSpikeTrap",
    "eventList": [
        {
            "id": "cf6f7106-6f56-4316-9452-57f0afaad173",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "c86cf363-9bb7-4cf5-a006-9c8fd4658596",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "4b2ed59f-e3ec-4367-a774-cae77ce7815b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "311b649e-d275-459b-bc0e-96f6df7b47ed",
    "visible": true
}