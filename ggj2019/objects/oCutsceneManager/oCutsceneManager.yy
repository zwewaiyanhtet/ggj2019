{
    "id": "5c74165f-0f21-44f6-ae08-c97e185e6881",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oCutsceneManager",
    "eventList": [
        {
            "id": "3f47f09f-3eef-4f62-ae17-9b24d889df13",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5c74165f-0f21-44f6-ae08-c97e185e6881"
        },
        {
            "id": "aa50e6d8-f815-41a6-9479-a4d9e399216f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "5c74165f-0f21-44f6-ae08-c97e185e6881"
        },
        {
            "id": "55b64ab0-5338-4d40-8d98-11d380ca2b0d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "5c74165f-0f21-44f6-ae08-c97e185e6881"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}