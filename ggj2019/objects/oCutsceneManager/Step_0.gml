switch cPos {

	case 0: 
	{
		times[0] -= 4;
	
		oPlayerCutscene.x += 4;
	
		if(times[0] <= 0) {
			oPlayerCutscene.image_index = 0;
			oPlayerCutscene.image_speed = 0;
			oPlayerHeadCutscene.image_index = 0;
			oPlayerHeadCutscene.image_speed = 0;
			
			cPos++;	
		}
		break;
	}
	case 1:
	{
		
		times[1] -= 1;
		if(!text1Created) {
			text1Created = true;		
			//text1 = instance_create_layer(oPlayerCutscene.x, oPlayerCutscene.y - 130, "Player", oText);
			//text1.text = "!";	
			text1 = instance_create_layer(oPlayerCutscene.x + 30, oPlayerCutscene.y - 200,"Player",oImgBubble);
			text1.the_sprite = sEx;
		}
		;
		if(times[1] <= 0) {
			instance_destroy(text1);
			text1Created = false;
			cPos++;	
		}
		break;
	} 
	case 2:
	{
		oPlayerCutscene.shell = noone;
		oShellCutscene.carrier = noone;
		oShellCutscene.isBeingCarried = false;
		
		times[2] -= 1;
		if(times[2] <= 0) {
			shellOriX = oShellCutscene.x;
			shellOriY = oShellCutscene.y;
			cPos++;	
		}
		break;
	}
	case 3:
	{
	
		times[3] -= 4;
		
		oPlayerCutscene.x += 4;
		
		oPlayerCutscene.image_speed = 1;
		oPlayerHeadCutscene.image_speed = 1;
	
		if(times[3] <= 0) {
			oPlayerCutscene.image_index = 0;
			oPlayerCutscene.image_speed = 0;
			oPlayerHeadCutscene.image_index = 0;
			oPlayerHeadCutscene.image_speed = 0;
			
			cPos++;	
		}
		break;
	}
	case 4:
	{
		times[4] -= 1;
		if(!text1Created) {
			text1Created = true;		
			//text1 = instance_create_layer(oPlayerCutscene.x, oPlayerCutscene.y - 130, "Player", oText);
			//text1.text = "!";	
			text1 = instance_create_layer(oPlayerCutscene.x + 30, oPlayerCutscene.y - 170,"Player",oImgBubble);
			text1.the_sprite = sHouseQuestion;
		}
		
		if(times[4] <= 0) {
			instance_destroy(text1);
			text1Created = false;
			cPos++;	
		}
		break;
	}
	case 5:
	{
		times[5] -= 1;
		if(times[5] <= 0) {
			cPos++;	
		}
		break;
	}
	case 6:
	{
		oPlayerCutscene.hDir = -1;
		
		oShellCutscene.x = choose(shellOriX - 1, shellOriX + 1);
		oShellCutscene.y = choose(shellOriY - 1, shellOriY + 1);
		
		times[6] -= 1;
		if(times[6] <= 0) {
			cPos++;	
		}
		break;
	}
	case 7:
	{
		times[7] -= 1;
		if(!text1Created) {
			text1Created = true;		
			//text1 = instance_create_layer(oPlayerCutscene.x, oPlayerCutscene.y - 130, "Player", oText);
			//text1.text = "!";	
			text1 = instance_create_layer(oPlayerCutscene.x + 30, oPlayerCutscene.y - 170,"Player",oImgBubble);	
			text1.the_sprite = sQuestion;
		}
		
		if(times[7] <= 0) {
			instance_destroy(text1);
			text1Created = false;
			cPos++;	
		}
		break;
	}
	case 8:
	{
		times[8] -= 1;
		if(!text1Created) {
			text1Created = true;		
			text1 = instance_create_layer(oShellCutscene.x, oShellCutscene.y - 130, "Player", oText);
			text1.text = "Do not worry, little one.";	
			//text1 = instance_create_layer(oPlayerCutscene.x + 30, oPlayerCutscene.y - 200,"Player",oImgBubble);				
		}
		
		if(times[8] <= 0) {
			instance_destroy(text1);
			text1Created = false;
			cPos++;	
		}
		break;
	}
	case 9:
	{
		times[9] -= 1;
		if(!text1Created) {
			text1Created = true;		
			text1 = instance_create_layer(oShellCutscene.x, oShellCutscene.y - 130, "Player", oText);
			text1.text = "You don't need \nto look anywhere.";	
			//text1 = instance_create_layer(oPlayerCutscene.x + 30, oPlayerCutscene.y - 200,"Player",oImgBubble);				
		}
		
		if(times[9] <= 0) {
			instance_destroy(text1);
			text1Created = false;
			cPos++;	
		}
		break;
	}
	case 10:
	{
		times[10] -= 1;
		if(!text1Created) {
			text1Created = true;		
			text1 = instance_create_layer(oShellCutscene.x, oShellCutscene.y - 130, "Player", oText);
			text1.text = "Because I am your . . .";	
			//text1 = instance_create_layer(oPlayerCutscene.x + 30, oPlayerCutscene.y - 200,"Player",oImgBubble);				
		}
		
		if(times[10] <= 0) {
			instance_destroy(text1);
			text1Created = false;
			cPos++;	
		}
		break;
	} case 11:
	{
		drawStart = true;
		percent = min(1, percent + max(((1-percent)/10), 0.005));
		//cPos++;
	}
}