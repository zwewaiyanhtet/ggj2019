{
    "id": "8990c7aa-6c9a-456c-985a-4a97c5e74fdb",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oPlayerCutscene",
    "eventList": [
        {
            "id": "183f1b65-3b11-4bf8-a4df-fbfd55421f1f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8990c7aa-6c9a-456c-985a-4a97c5e74fdb"
        },
        {
            "id": "c977bc08-3457-45b3-8273-b44deb6d4b0b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "8990c7aa-6c9a-456c-985a-4a97c5e74fdb"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2a8f440d-9732-4dcc-a4b9-107a365ffbc3",
    "visible": true
}