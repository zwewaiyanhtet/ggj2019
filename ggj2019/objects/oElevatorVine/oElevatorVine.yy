{
    "id": "2341c8a5-144f-4fea-9fbe-aa315fd9f4a1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oElevatorVine",
    "eventList": [
        {
            "id": "af0b5fbc-0e6b-47f6-8d56-b5fbe0fccaa8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2341c8a5-144f-4fea-9fbe-aa315fd9f4a1"
        },
        {
            "id": "92d3e2c9-5986-47b6-85f3-b605bfa68fe3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "2341c8a5-144f-4fea-9fbe-aa315fd9f4a1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "81fb152e-28c3-4ea3-8be6-9fa971a55191",
    "visible": true
}