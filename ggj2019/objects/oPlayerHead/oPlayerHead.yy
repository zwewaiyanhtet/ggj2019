{
    "id": "27e7eeb0-05e4-4358-8a29-664ecec8c1bc",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oPlayerHead",
    "eventList": [
        {
            "id": "cd6f7af1-6632-4d48-888a-f22cee3b0317",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "27e7eeb0-05e4-4358-8a29-664ecec8c1bc"
        },
        {
            "id": "f0d870e5-710a-4e9c-a031-69232f51e08f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "27e7eeb0-05e4-4358-8a29-664ecec8c1bc"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c9309554-7955-4fd5-8c6e-e8f80ca56bfe",
    "visible": true
}