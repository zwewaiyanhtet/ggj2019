{
    "id": "f6513934-139a-4f9b-8fa8-b2246dff6c6c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oSpikeSpwaner",
    "eventList": [
        {
            "id": "78269b05-683f-4add-80e6-4121e4e4e2c9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f6513934-139a-4f9b-8fa8-b2246dff6c6c"
        },
        {
            "id": "c2a48f18-94e2-49a5-b623-63edff260824",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "f6513934-139a-4f9b-8fa8-b2246dff6c6c"
        },
        {
            "id": "220f7081-b9eb-4cbe-8450-0953b6fdafb4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "f6513934-139a-4f9b-8fa8-b2246dff6c6c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}