if(horizontal) {
	direction = 90;
	image_angle = 90;
}

if(opened) {
	sprite_index = sDoorOpened;
	image_speed = 0;
	image_index = image_number - 1;
} else {
	sprite_index = sDoorClosed;
	image_speed = 0;
	image_index = image_number - 1;
}