if(keyboard_check_pressed(ord("M"))) {
	opened = !opened;
}

if(opened) {
	if(sprite_index == sDoorClosed) {
		sprite_index = sDoorOpened;
		image_index = 0;
		image_speed = 1;
	}
} else {
	if(sprite_index == sDoorOpened) {
		sprite_index = sDoorClosed;
		image_index = 0;
		image_speed = 1;
	}
}
