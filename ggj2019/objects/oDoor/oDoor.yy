{
    "id": "c6ee330f-9db7-43c9-9eb6-dd63518969df",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oDoor",
    "eventList": [
        {
            "id": "091ce09b-ffb3-4cb9-b625-4d58104e836a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c6ee330f-9db7-43c9-9eb6-dd63518969df"
        },
        {
            "id": "0372dd60-ef15-4353-a6a3-286c3b4fc13d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c6ee330f-9db7-43c9-9eb6-dd63518969df"
        },
        {
            "id": "5fc3cbcd-11bb-4873-89d2-8f51f1297d80",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "c6ee330f-9db7-43c9-9eb6-dd63518969df"
        },
        {
            "id": "c4e228ef-5ff6-418e-9437-612ccbe8691a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "c6ee330f-9db7-43c9-9eb6-dd63518969df"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "3784ce46-7049-44bc-ac66-d883935e13ec",
    "visible": true
}