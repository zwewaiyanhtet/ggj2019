{
    "id": "6bbd2182-ffc7-4e3b-8b53-a58d105e714d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oStartMenu",
    "eventList": [
        {
            "id": "b4d90084-5648-4767-af09-5078b5f01b40",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6bbd2182-ffc7-4e3b-8b53-a58d105e714d"
        },
        {
            "id": "87c75c85-4294-4f65-84a2-9789ac931bcd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "6bbd2182-ffc7-4e3b-8b53-a58d105e714d"
        },
        {
            "id": "48f22d2f-e0ef-4adf-81b9-6ebe7ea360e5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "6bbd2182-ffc7-4e3b-8b53-a58d105e714d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}