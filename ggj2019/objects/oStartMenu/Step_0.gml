// Animation menu fade in

menu_x += (menu_x_target - menu_x) / menu_speed;

// Keyboard Control

if(menu_control){
	if(keyboard_check_pressed(vk_up) || keyboard_check_pressed(ord("W"))){
		menu_cursor++; 
		if(menu_cursor >= menu_items){
			menu_cursor = 0;
		}
		soundPlay(sndMenuChoose);
	}else if(keyboard_check_pressed(vk_down) || keyboard_check_pressed(ord("S"))){
		menu_cursor--;
		if(menu_cursor < 0){
			menu_cursor = menu_items-1;
		}
		soundPlay(sndMenuChoose);
	}else if(keyboard_check_pressed(vk_enter) || keyboard_check_pressed(vk_space)){
		menu_committed = menu_cursor;
		menu_control = false;
		switch(menu_committed){
			case 1:
			room_goto(rmGame);
			break;
			case 0:
			game_end();
			break;
		}
		
	}
}