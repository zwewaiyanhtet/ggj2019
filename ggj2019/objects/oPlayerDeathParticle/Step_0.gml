if(goToShell) {
	direction = point_direction(x, y, oShell.x, oShell.y - 50);
	
	var d = point_distance(x, y, oShell.x, oShell.y);
	
	speed = d * 0.1;
	
	if(d < 50) {
		instance_destroy();	
	}
} else {
	speed -= speed * 0.05;	
}