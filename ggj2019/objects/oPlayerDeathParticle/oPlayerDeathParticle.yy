{
    "id": "fd85cb49-712e-479c-ba73-4afd3cd496ee",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oPlayerDeathParticle",
    "eventList": [
        {
            "id": "f0fa03ec-a2e7-42c5-9ebe-a8396a293ab5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "fd85cb49-712e-479c-ba73-4afd3cd496ee"
        },
        {
            "id": "dc63c9a0-bae9-4745-ac09-e180fc0648da",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "fd85cb49-712e-479c-ba73-4afd3cd496ee"
        },
        {
            "id": "c9ed7e7e-de9f-4f1c-b0b8-af6701e008bb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "fd85cb49-712e-479c-ba73-4afd3cd496ee"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "0a039868-326e-419a-8963-2c04523c3698",
    "visible": true
}