{
    "id": "bdecf3ba-9420-4614-b6e4-bf5717d01855",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oHandle",
    "eventList": [
        {
            "id": "8b7e27da-d6bc-42c2-92ef-34edd9d13563",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "bdecf3ba-9420-4614-b6e4-bf5717d01855"
        },
        {
            "id": "5278b969-63ba-4989-b7bf-0fcb7ec52084",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "bdecf3ba-9420-4614-b6e4-bf5717d01855"
        },
        {
            "id": "eb9045b9-1402-4402-ab6e-fcdfa5fce2c2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "bdecf3ba-9420-4614-b6e4-bf5717d01855"
        },
        {
            "id": "4be0e31f-95d6-45f3-91cb-e3ab73e75a60",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "bdecf3ba-9420-4614-b6e4-bf5717d01855"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a3d5cd30-e232-4d6b-a352-102871030e86",
    "visible": true
}