if(!instance_exists(oPlayer)) exit;

var inputAction = inputGetAction();

if(inputAction && place_meeting(x, y, oPlayer))
{
	if(target != noone)
	{
		// action if the handle's target is an elevator
		targetName = object_get_name(target.object_index);
		if(targetName == "oElevator")
		{

			target.moving = !target.moving;

			
			// added handle's animation
			if(sprite_index == sHandleClose) {
				sprite_index = sHandle;
				soundPlay(global.switchActivateSound);
			} else {
				sprite_index = sHandleClose;
				soundPlay(global.switchDeactivateSound);
			}
			image_index = 0;
			image_speed = 1;
		} 
		else if( targetName == "oDoor")
		{
			target.opened = ! target.opened;
			// added handle's animation
			if(sprite_index == sHandleClose) {
				sprite_index = sHandle;
				soundPlay(global.switchActivateSound);
			} else {
				sprite_index = sHandleClose;
				soundPlay(global.switchDeactivateSound);
			}
			image_index = 0;
			image_speed = 1;
		}
	}	
}