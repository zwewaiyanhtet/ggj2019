{
    "id": "5dcf2163-2db7-4c36-a434-04e79e827038",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oPressureSwitch",
    "eventList": [
        {
            "id": "f0bd2a87-a16b-4f35-8137-10492c979fa8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5dcf2163-2db7-4c36-a434-04e79e827038"
        },
        {
            "id": "d2f0aa2c-254b-44af-9ccb-08283148256e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "5dcf2163-2db7-4c36-a434-04e79e827038"
        },
        {
            "id": "9609463f-1e41-482a-b672-fe8ada864dea",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "5dcf2163-2db7-4c36-a434-04e79e827038"
        },
        {
            "id": "9ce73839-3fbb-41b9-b3f6-788837818009",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "5dcf2163-2db7-4c36-a434-04e79e827038"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "fabab2be-f10d-42af-b135-6e39387af355",
    "visible": true
}