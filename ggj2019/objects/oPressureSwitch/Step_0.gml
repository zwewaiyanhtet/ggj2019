if(!instance_exists(oPlayer)) exit;

if(target != noone)
{
	targetName = object_get_name(target.object_index);
	// action if the handle's target is an elevator
	if(targetName == "oElevator")
	{	
		if(place_meeting(x, y, oPlayer) || place_meeting(x, y, oShell))
		{
			target.moving = true;
		} else {
			target.moving = false;	
		}
	} else if (targetName == "oDoor")
	{
		if(place_meeting(x, y, oPlayer) || place_meeting(x, y, oShell))
		{
			target.opened = true;
		} else {
			target.opened = false;	
		}
	}
}	

//animation
if(place_meeting(x, y, oPlayer) || place_meeting(x, y, oShell))
{
	if(sprite_index == sPressureSwitchOff) {
		sprite_index = sPressureSwitch;
		image_index = 0;
		image_speed = 1;
	}
} else {
	if(sprite_index == sPressureSwitch) {
		sprite_index = sPressureSwitchOff;
		image_index = 0;
		image_speed = 1;
	}
}
