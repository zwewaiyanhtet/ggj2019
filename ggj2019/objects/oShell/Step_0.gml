// gravity

if(!isBeingCarried) {
	var eleInst = collision_line(x - halfSpriteWidth, y, x + halfSpriteWidth, y, oElevator, false, true);
	if eleInst != noone {
		moveY = 0;
	
		if(eleInst.moving) {
			y += (eleInst.moveSpeed * sign(eleInst.vDir));
		} else {
			while place_meeting(x, y - 1, oElevator) {
				y -= 1;	
			}
		}
	
	} 
	else {
		moveY += grav;
		if moveY > 0 {
			if place_meeting(x, y+abs(moveY), oGround) {
				while !place_meeting(x, y+1, oGround) {
					y += 1;
				}
				moveY = 0;
			}
		
		}
	}
	if(carrier != noone) {
		depth = carrier.depth + 1;
	}
	
	var door = instance_place(x, y+abs(moveY), oDoor);
	if(door != noone)    
	{
		var doorOpened = door.opened && door.sprite_index == sDoorOpened && door.image_index == door.image_number - 1;
		if(door.horizontal && !doorOpened){
			moveY = 0;
		}
	}
	
	y += moveY;
} else {
	if(instance_exists(carrier)) {
		x = carrier.x;
		y = carrier.y;
		
		image_xscale = carrier.hDir;
		hDir = carrier.hDir
		depth = carrier.depth - 1;
	}
}

forwardCollisionPoint.x = x + (50 * hDir);
forwardCollisionPoint.y = y - 80;

