{
    "id": "cc79ce37-d18d-4875-b5a6-27909e6ea6a0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oShell",
    "eventList": [
        {
            "id": "7f9382f6-1801-4f27-8a3a-73285b25b0a3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "cc79ce37-d18d-4875-b5a6-27909e6ea6a0"
        },
        {
            "id": "ed232a53-7378-4a43-b974-0ca9c66d9b50",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "cc79ce37-d18d-4875-b5a6-27909e6ea6a0"
        },
        {
            "id": "ed7019da-e1c4-4680-81d6-7e90cd9df0bd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "cc79ce37-d18d-4875-b5a6-27909e6ea6a0"
        },
        {
            "id": "a47a35d2-aa27-4bbe-b497-e2aa837d2835",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "cc79ce37-d18d-4875-b5a6-27909e6ea6a0"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a7c6f7c1-4ddb-4bc6-abc7-896f6c35d9bb",
    "visible": true
}