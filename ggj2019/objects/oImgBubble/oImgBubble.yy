{
    "id": "ab37baab-ebbe-413d-8862-36c4af7745c4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oImgBubble",
    "eventList": [
        {
            "id": "9bbbe001-0064-4960-8c9e-1e0cd958c6de",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ab37baab-ebbe-413d-8862-36c4af7745c4"
        },
        {
            "id": "b3fcbed7-e1d1-484e-9865-391002b3c250",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "ab37baab-ebbe-413d-8862-36c4af7745c4"
        },
        {
            "id": "c37e89b9-df57-4e29-813f-fbf2396ccbc0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "ab37baab-ebbe-413d-8862-36c4af7745c4"
        },
        {
            "id": "7e3ca3e7-f85a-4bf9-8b0a-b4ebbed1cde4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "ab37baab-ebbe-413d-8862-36c4af7745c4"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f516dfe9-4c12-4cef-b701-8442de3ac5d5",
    "visible": true
}