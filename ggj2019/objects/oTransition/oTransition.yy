{
    "id": "37f190ba-135b-4ec7-a640-763e16482621",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oTransition",
    "eventList": [
        {
            "id": "1f7e88d4-212e-440f-a5fc-e2608f24fd3e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "c86cf363-9bb7-4cf5-a006-9c8fd4658596",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "37f190ba-135b-4ec7-a640-763e16482621"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b3c1a7a5-bf0f-435a-8dd6-2b756a8da85b",
    "visible": true
}