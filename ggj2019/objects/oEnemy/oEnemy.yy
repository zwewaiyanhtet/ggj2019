{
    "id": "a421831e-8283-4d84-9b2c-81e0aa81e8b0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oEnemy",
    "eventList": [
        {
            "id": "7e09f700-67bb-4fe8-94c6-37424dddd090",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a421831e-8283-4d84-9b2c-81e0aa81e8b0"
        },
        {
            "id": "ad4bdceb-f275-4eba-8801-a0b563a7bab7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "a421831e-8283-4d84-9b2c-81e0aa81e8b0"
        },
        {
            "id": "8d318c95-929e-4c69-9c82-ee394e6a3d7e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "c86cf363-9bb7-4cf5-a006-9c8fd4658596",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "a421831e-8283-4d84-9b2c-81e0aa81e8b0"
        },
        {
            "id": "9c776756-9f00-4601-a721-7ae02207357a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "a421831e-8283-4d84-9b2c-81e0aa81e8b0"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c9bf2788-959f-470c-9a9e-1e311e56dd07",
    "visible": true
}