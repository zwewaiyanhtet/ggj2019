{
    "id": "4827ab31-c3be-4902-a457-ddaf0f9343f7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oElevator",
    "eventList": [
        {
            "id": "acd509e9-37d3-426b-aff1-f9c2a8b34ad6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4827ab31-c3be-4902-a457-ddaf0f9343f7"
        },
        {
            "id": "c8806655-f682-4293-818d-420f241f760d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "4827ab31-c3be-4902-a457-ddaf0f9343f7"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "cc5da4ed-7c1f-42ad-9ee1-e0032d587d96",
    "visible": true
}