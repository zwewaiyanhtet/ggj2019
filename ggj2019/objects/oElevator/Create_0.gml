moveDistance = 200;
distanceLeft = moveDistance;
vDir = -1;

moving = false;
moveSpeed = 2;

handle = noone;

vine1 = instance_create_depth(x, y, depth + 1, oElevatorVine);
vine1.actor = id;
vine1.offsetX = -80;

vine2 = instance_create_depth(x, y, depth + 1, oElevatorVine);
vine2.actor = id;
vine2.offsetX = 80;
