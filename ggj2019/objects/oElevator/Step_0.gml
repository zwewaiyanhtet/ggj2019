toggle = keyboard_check_pressed(ord("L"));

if(toggle) {
	moving = true;
}

if(moving) {
	distanceLeft -= moveSpeed;

	var moveY = y + (moveSpeed * sign(vDir));
	
	if(distanceLeft <= 0) {
		moveY += (distanceLeft * -1) * sign(vDir);
		
		//moving = false;
		distanceLeft = moveDistance;
		vDir = vDir * -1;
	}
	
	y = moveY;
	
	if(handle != noone) {
		if(handle.moveWithElevator) {
			var hMoveY = handle.y + (moveSpeed * sign(vDir));
	
			if(distanceLeft <= 0) {
				hMoveY += (distanceLeft * -1) * sign(vDir);
			}
	
			handle.y = hMoveY;
		}
	}
}