partSystem = part_system_create();

partType = part_type_create();
part_type_sprite(partType, sSpikeBrokeParticle, false, false, false);
part_type_scale(partType, .8, 1.2);
part_type_life(partType, room_speed*.8, room_speed*1.2);
part_type_orientation(partType, 0, 359, 0, 0, true);
part_type_speed(partType, .8, 2, 0, false);
part_type_direction(partType, 0, 180, 0, false);
part_type_gravity(partType, 0.20, 270);

partEmitter = part_emitter_create(partSystem);
part_emitter_region(partSystem, partEmitter, -12, 12, -12, 12, ps_shape_ellipse, ps_distr_gaussian);