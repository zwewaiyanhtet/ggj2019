isEnabled = true
moveSpeedWithShell = 5;
moveSpeedWithoutShell = 8;
jumpSpeed = -20
grav = .75

inputHor = 0
moveX = 0
moveY = 0
hDir = 1
movable = true;

halfSpriteWidth = sprite_width * 0.1;

shell = noone;

head = instance_create_depth(x, y, depth - 2, oPlayerHead);
head.actor = id;

playerRespwarning = true;
head.visible = false;