repeat(10) {

	with(instance_create_depth(x, y, depth + 3, oPlayerDeathParticle)) {
		direction = irandom_range(0, 359);
		speed = irandom_range(1,5);
		var s = choose(0.8, 1, 1.2, 1.4, 1.6);
		image_xscale = s;
		image_yscale = s;
	}

}

shell.carrier = noone;
shell.alarm[0] = 100;
soundPlay(global.playerDeathSound);
instance_destroy(head);