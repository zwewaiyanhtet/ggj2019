if !isEnabled exit;


/// horizontal movement
var inputLeft = inputGetLeft();
var inputRight = inputGetRight();
inputHor = inputRight - inputLeft;

moveX = inputHor * (shell.isBeingCarried ? moveSpeedWithShell : moveSpeedWithoutShell);
if place_meeting(x+moveX + (sign(moveX) * 5), y, oGround) {
	//while !place_meeting(x+sign(moveX), y, oGround) {
	//	x += sign(moveX);
	//}
	moveX = 0;
}

//check wall collision
var door = instance_place(x+moveX + (sign(moveX) * 5), y, oDoor);
if(door != noone) {
	var doorOpened = door.opened && door.sprite_index == sDoorOpened && door.image_index == door.image_number - 1;
	if(!door.horizontal && !doorOpened) {
	 moveX = 0;	
	}
}

if inputHor != 0 && inputHor != hDir { 
	hDir = sign(inputHor);
	//if eyes != noone eyes.hdir = hdir;
}
image_xscale = hDir;

if(!movable) {
	moveX = 0;	
}
x += moveX;

// moving down
var eleInst = collision_line(x - halfSpriteWidth, y, x + halfSpriteWidth, y, oElevator, false, true);
door = instance_place(x, y+abs(moveY), oDoor);
if eleInst != noone {
	moveY = 0;
	
	while place_meeting(x, y - 1, oElevator) {
			y -= 1;	
	}
	
	if(eleInst.moving) {
		y += (eleInst.moveSpeed * sign(eleInst.vDir));
	} 
	//else {
		
	//}
	
} 
else {
	moveY += grav;
	if moveY > 0 {
		if place_meeting(x, y+abs(moveY), oGround) {
			while !place_meeting(x, y+1, oGround) {
				y += 1;
			}
			moveY = 0;
		} 
	}
}

if(door != noone)
{
	var doorOpened = door.opened && door.sprite_index == sDoorOpened && door.image_index == door.image_number - 1;
	if(door.horizontal && !doorOpened){
		moveY = 0;
	}
}

y += moveY;


// Shell movement
var inputCarry = inputGetCarry();

if(inputCarry) {
	
	if(shell.isBeingCarried) {
		shell.isBeingCarried = false;	
		soundPlay(global.playerPopOutSound);
	} else {
		if(place_meeting(x, y, shell)) {
			shell.isBeingCarried = true;
			soundPlay(global.playerPopInSound);
		}
	}
	
}

//animation
if(playerRespwarning) {
	image_speed = 1;
} else {
	
	if(inputHor != 0) {
		image_speed = 1;
		head.image_speed = 1;
	} else {
		image_speed = 0;
		image_index = 0;
		head.image_speed =0;
		head.image_index = 0;
	}

}