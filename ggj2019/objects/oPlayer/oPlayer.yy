{
    "id": "c86cf363-9bb7-4cf5-a006-9c8fd4658596",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oPlayer",
    "eventList": [
        {
            "id": "5f3dc9ea-ca45-430e-a192-bbcc667ba2ea",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c86cf363-9bb7-4cf5-a006-9c8fd4658596"
        },
        {
            "id": "d60f4d20-a316-42ed-9e83-f3c4b8bef367",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c86cf363-9bb7-4cf5-a006-9c8fd4658596"
        },
        {
            "id": "1283063b-a73b-4e05-95ba-e6a7a1bd82bb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "c86cf363-9bb7-4cf5-a006-9c8fd4658596"
        },
        {
            "id": "ec31e67e-fe59-4ea2-be56-7da91a7c254c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "c86cf363-9bb7-4cf5-a006-9c8fd4658596"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2fc5cd41-27fc-4498-9ba3-0c29c20e180e",
    "visible": true
}