if(movable) {
	moveY += grav;
	if moveY > 0 {
		if place_meeting(x, y+abs(moveY), oGround) {
			while !place_meeting(x, y, oGround) {
				y += 1;
			}
			moveY = 0;
		} 
	}
	y += moveY;
} else {
	if(sprite_index == sSpike) && (instance_exists(oPlayer) && (!movable)) 
	{
		var distanceWithPlayer = abs(x - oPlayer.x);
		var isUnder = sign(y - oPlayer.y) < 0;
		
		if(distanceWithPlayer < 200) && isUnder 
		{
			sprite_index = sSpikeJiggle;
			alarm[0] = jingleTime;
		}
	}
}