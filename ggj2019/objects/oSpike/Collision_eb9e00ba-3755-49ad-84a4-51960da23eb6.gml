var closeX = lengthdir_x(90, point_direction(x, y, other.x, other.y));
var closeY = lengthdir_y(90, point_direction(x, y, other.x, other.y));
spikePartsPlay(x+closeX, y+closeY);
soundPlay(global.spikeDropSound);

instance_destroy();
