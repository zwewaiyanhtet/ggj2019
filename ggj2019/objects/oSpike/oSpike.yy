{
    "id": "55b1f310-ce51-4412-8279-bee10887d699",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oSpike",
    "eventList": [
        {
            "id": "34b3fc64-9afe-452d-a249-d67f5302a0c5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "55b1f310-ce51-4412-8279-bee10887d699"
        },
        {
            "id": "e64fa9c5-c613-436c-9d9c-e90ce4da6c34",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "55b1f310-ce51-4412-8279-bee10887d699"
        },
        {
            "id": "8bee7b13-b2d4-4e5a-b4a3-e8d70a8f7f4c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "d677ab05-ceaf-4486-86b8-85ee70fba8e1",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "55b1f310-ce51-4412-8279-bee10887d699"
        },
        {
            "id": "e62d91f7-b74f-4b68-a5d2-972a00c31230",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "55b1f310-ce51-4412-8279-bee10887d699"
        },
        {
            "id": "eb9e00ba-3755-49ad-84a4-51960da23eb6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "cc79ce37-d18d-4875-b5a6-27909e6ea6a0",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "55b1f310-ce51-4412-8279-bee10887d699"
        },
        {
            "id": "375816d8-dda3-4570-9b06-5615a86ebd26",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "c86cf363-9bb7-4cf5-a006-9c8fd4658596",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "55b1f310-ce51-4412-8279-bee10887d699"
        }
    ],
    "maskSpriteId": "d85088b8-3318-4193-b6e2-327898438881",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d85088b8-3318-4193-b6e2-327898438881",
    "visible": true
}