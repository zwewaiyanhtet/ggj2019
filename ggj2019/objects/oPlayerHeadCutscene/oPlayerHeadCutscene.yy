{
    "id": "872a0b3a-5501-40fd-8132-55e6e75810c8",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oPlayerHeadCutscene",
    "eventList": [
        {
            "id": "c78fe860-8edb-4bba-a0bc-8150a64cecaa",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "872a0b3a-5501-40fd-8132-55e6e75810c8"
        },
        {
            "id": "8ff3923b-fdad-425b-9975-6b7ca8fe4d8a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "872a0b3a-5501-40fd-8132-55e6e75810c8"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c9309554-7955-4fd5-8c6e-e8f80ca56bfe",
    "visible": true
}