{
    "id": "e7fa0f99-40fc-43aa-9514-6d2260f43e70",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oShellCutscene",
    "eventList": [
        {
            "id": "92de1a6d-3ad2-4ba3-baf5-4a6e52026096",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e7fa0f99-40fc-43aa-9514-6d2260f43e70"
        },
        {
            "id": "bd9f88e3-a315-41e7-8fbe-aaa3706a5e26",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e7fa0f99-40fc-43aa-9514-6d2260f43e70"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a7c6f7c1-4ddb-4bc6-abc7-896f6c35d9bb",
    "visible": true
}