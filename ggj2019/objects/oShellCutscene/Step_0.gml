// gravity

if(!isBeingCarried) {

		moveY += grav;
		if moveY > 0 {
			if place_meeting(x, y+abs(moveY), oGround) {
				while !place_meeting(x, y+1, oGround) {
					y += 1;
				}
				moveY = 0;
			}
		
		}
	if(carrier != noone) {
		depth = carrier.depth + 1;
	}
	
	y += moveY;
} else {
	if(instance_exists(carrier)) {
		x = carrier.x;
		y = carrier.y;
		
		image_xscale = carrier.hDir;
		hDir = carrier.hDir
		depth = carrier.depth - 1;
	}
}
